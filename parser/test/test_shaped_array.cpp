#include <shape.hpp>

int main()
{
	bool notFailed = true;

	sjp::vector::ShapedArray<int, 2, 2> array;

	notFailed &= array.shape.size == array.data().size();

	array(0,0) = 0;
	array(0,1) = 1;
	array(1,0) = 10;
	array(1,1) = 11;

	notFailed &= array.data()[0] == 0;
	notFailed &= array.data()[1] == 1;
	notFailed &= array.data()[2] == 10;
	notFailed &= array.data()[3] == 11;

	array.data()[3] = 22;
	notFailed &= array(1,1) == 22;

	return !notFailed;
}
