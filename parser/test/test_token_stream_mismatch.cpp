#include <token_stream.hpp>

#include <logging.h>

#include <initializer_list>
#include <sstream>
#include <string>

int main()
{
	using namespace sjp;

	logging::setupLoggingNoFilterClog();

	bool notFailed = true;

	std::istringstream is("one two");
	const auto idString = [](const std::string& x) { return x; };

	TokenStream<std::string, decltype(idString), decltype(idString)> ts(is, idString, idString, std::string("ERROR"));

	std::initializer_list<std::pair<std::string, std::string>> expectedAndActuals =
		{
			{"alpha", "one"},
			{"beta", "two"},
		};

	for (const auto& expectedAndActual : expectedAndActuals)
	{
		try
		{
			ts.matchToken(expectedAndActual.first);
		}
		catch (const TokenMismatch<std::string>& mismatch)
		{
			notFailed &= mismatch.expectedToken == expectedAndActual.first;
			notFailed &= mismatch.actualString == expectedAndActual.second;
		}
	}

	return !notFailed;
}
