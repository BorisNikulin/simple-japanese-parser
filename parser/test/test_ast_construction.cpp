#include <ast.h>

#include <logging.h>
#include <token.h>
#include <token_stream.hpp>

#include <sstream>
#include <memory>

namespace test
{
	using TokenStream = sjp::TokenStream<sjp::Token>;

	std::unique_ptr<sjp::ast::Noun> parseNoun(TokenStream& ts)
	{
		std::unique_ptr<sjp::ast::Noun> noun(new sjp::ast::Noun());

		ts.matchToken(sjp::Token::WORD1);
		noun->type = sjp::ast::Noun::Type::WORD1;
		noun->value = ts.matchedString();

		return noun;
	}

	std::unique_ptr<sjp::ast::Verb> parseVerb(TokenStream& ts)
	{
		ts.matchToken(sjp::Token::WORD2);
		return std::unique_ptr<sjp::ast::Verb>(new sjp::ast::Verb{ts.matchedString()});
	}

	std::unique_ptr<sjp::ast::Tense> parseTense(TokenStream& ts)
	{
		ts.matchToken(sjp::Token::VERB);
		return std::unique_ptr<sjp::ast::Tense>(new sjp::ast::Tense{sjp::ast::Tense::Type::VERB, ts.matchedString()});
	}

	std::unique_ptr<sjp::ast::AfterObject> parseAfterObject(TokenStream& ts)
	{
		std::unique_ptr<sjp::ast::AfterObject> afterObject;

		sjp::ast::AfterObject::Verb afterObjectVerb;

		afterObjectVerb.verb = parseVerb(ts);
		afterObjectVerb.tense = parseTense(ts);

		ts.matchToken(sjp::Token::PERIOD);

		afterObject = std::unique_ptr<sjp::ast::AfterObject>(new sjp::ast::AfterObject(std::move(afterObjectVerb)));

		return afterObject;
	}

	std::unique_ptr<sjp::ast::AfterNoun> parseAfterNoun(TokenStream& ts)
	{
		std::unique_ptr<sjp::ast::AfterNoun> afterNoun;

		sjp::ast::AfterNoun::Object afterNounObject;

		ts.matchToken(sjp::Token::OBJECT);
		afterNounObject.object = ts.matchedString();

		afterNounObject.afterObject = parseAfterObject(ts);

		afterNoun = std::unique_ptr<sjp::ast::AfterNoun>(new sjp::ast::AfterNoun(std::move(afterNounObject)));

		return afterNoun;
	}

	std::unique_ptr<sjp::ast::AfterSubject> parseAfterSubject(TokenStream& ts)
	{
		std::unique_ptr<sjp::ast::AfterSubject> afterSubject;

		sjp::ast::AfterSubject::Noun afterSubjectNoun;

		afterSubjectNoun.noun = parseNoun(ts);
		afterSubjectNoun.afterNoun = parseAfterNoun(ts);

		afterSubject = std::unique_ptr<sjp::ast::AfterSubject>(new sjp::ast::AfterSubject(std::move(afterSubjectNoun)));

		return afterSubject;
	}

	std::unique_ptr<sjp::ast::S> parseS(TokenStream& ts)
	{
		std::unique_ptr<sjp::ast::S> s(new sjp::ast::S());

		ts.matchToken(sjp::Token::CONNECTOR);
		s->hasConnector = true;
		s->connector = ts.matchedString();

		s->noun = parseNoun(ts);

		ts.matchToken(sjp::Token::SUBJECT);
		s->subject = ts.matchedString();

		s->afterSubject = parseAfterSubject(ts);

		return s;
	}

	/** Parsed a fixed grammar sentence into an AST.
	 *
	 *  The grammar it parses is
	 *  \verbatim
			<story>
				=> <s> EOFM
				=> CONNECTOR <noun> SUBJECT <afterSubject> EOFM
				=> CONNECTOR WORD1 SUBJECT <afterSubject> EOFM
				=> CONNECTOR WORD1 SUBJECT <noun> <afterNoun> EOFM
				=> CONNECTOR WORD1 SUBJECT WORD1 <afterNoun> EOFM
				=> CONNECTOR WORD1 SUBJECT WORD1 OBJECT <afterObject> EOFM
				=> CONNECTOR WORD1 SUBJECT WORD1 OBJECT <verb> <tense> PERIOD EOFM
				=> CONNECTOR WORD1 SUBJECT WORD1 OBJECT WORD2 <tense> PERIOD EOFM
				=> CONNECTOR WORD1 SUBJECT WORD1 OBJECT WORD2 VERB PERIOD EOFM
	    \endverbatim
	 */
	sjp::ast::Story parse(TokenStream ts)
	{
		sjp::ast::Story story(parseS(ts));

		ts.matchToken(sjp::Token::EOFM);

		return story;
	}
}

int main()
{
	sjp::logging::setupLoggingNoFilterClog();

	bool notFailed = true;

	std::istringstream is("mata borisu wa omoshiroi o kakoI masu . eofm");
	auto scanner = [](const std::string& word){ return sjp::scan(word).first; };

	sjp::TokenStream<sjp::Token>
		ts(is, *scanner, sjp::tokenToString, sjp::Token::ERROR);

	sjp::ast::Story story = test::parse(ts);

	notFailed &= story.ss.size() == 1;

	auto& s = story.ss.front();
	notFailed &= s->hasConnector == true;
	notFailed &= s->connector == "mata";
	notFailed &= s->noun->type == sjp::ast::Noun::Type::WORD1;
	notFailed &= s->noun->value == "borisu";
	notFailed &= s->subject == "wa";

	auto& afterSubject = s->afterSubject;
	notFailed &= afterSubject->type == sjp::ast::AfterSubject::Type::NOUN;
	notFailed &= afterSubject->noun.noun->type == sjp::ast::Noun::WORD1;
	notFailed &= afterSubject->noun.noun->value == "omoshiroi";

	auto& afterNoun = afterSubject->noun.afterNoun;
	notFailed &= afterNoun->type == sjp::ast::AfterNoun::Type::OBJECT;
	notFailed &= afterNoun->object.object == "o";

	auto& afterObject = afterNoun->object.afterObject;
	notFailed &= afterObject->type == sjp::ast::AfterObject::Type::VERB;
	notFailed &= afterObject->verb.verb->value == "kakoI";
	notFailed &= afterObject->verb.tense->type == sjp::ast::Tense::Type::VERB;
	notFailed &= afterObject->verb.tense->value == "masu";

	return !notFailed;
}
