#include <ast.h>

#include <sstream>
#include <memory>
#include <iostream>

namespace test
{
	using namespace sjp::ast;

	template<typename T>
	using uptr = std::unique_ptr<T>;


	uptr<Noun> nounWord1()
	{
		return uptr<Noun>(new Noun{Noun::Type::WORD1, "nounWord1"});
	}

	uptr<Noun> nounPronoun()
	{
		return uptr<Noun>(new Noun{Noun::Type::PRONOUN, "nounPronoun"});
	}

	uptr<Verb> verb()
	{
		return uptr<Verb>(new Verb{"verb"});
	}

	uptr<Be> beIs()
	{
		return uptr<Be>(new Be{Be::Type::IS, "beIs"});
	}

	uptr<Be> beWas()
	{
		return uptr<Be>(new Be{Be::Type::WAS, "beWas"});
	}

	uptr<Tense> tenseVerb()
	{
		return uptr<Tense>(new Tense{Tense::Type::VERB, "tenseVerb"});
	}

	uptr<Tense> tenseVerbNeg()
	{
		return uptr<Tense>(new Tense{Tense::Type::VERB_NEG, "tenseVerbNeg"});
	}

	uptr<Tense> tenseVerbPast()
	{
		return uptr<Tense>(new Tense{Tense::Type::VERB_PAST, "tenseVerbPast"});
	}

	uptr<Tense> tenseVerbPastNeg()
	{
		return uptr<Tense>(new Tense{Tense::Type::VERB_PAST_NEG, "tenseVerbPastNeg"});
	}

	Story createAst()
	{
		uptr<AfterSubject> afterSubject1(new AfterSubject(AfterSubject::Verb{verb(), tenseVerb()}));
		uptr<S> s1(new S{true, "connector", nounWord1(), "subject", std::move(afterSubject1)});
		Story story(std::move(s1));

		uptr<AfterNoun> afterNoun1(new AfterNoun(AfterNoun::Be{beIs()}));
		uptr<AfterSubject> afterSubject2(new AfterSubject(AfterSubject::Noun{nounPronoun(), std::move(afterNoun1)}));
		uptr<S> s2(new S{true, "connectoR", nounPronoun(), "subjecT", std::move(afterSubject2)});
		story.ss.push_back(std::move(s2));

		uptr<AfterNoun> afterNoun2(new AfterNoun(AfterNoun::Destination{"destination", verb(), tenseVerbNeg()}));
		uptr<AfterSubject> afterSubject3(new AfterSubject(AfterSubject::Noun{nounPronoun(), std::move(afterNoun2)}));
		uptr<S> s3(new S{true, "connectOR", nounPronoun(), "subjeCT", std::move(afterSubject3)});
		story.ss.push_back(std::move(s3));

		uptr<AfterObject> afterObject1(new AfterObject(AfterObject::Verb{verb(), tenseVerbPast()}));
		uptr<AfterNoun> afterNoun3(new AfterNoun(AfterNoun::Object{"object", std::move(afterObject1)}));
		uptr<AfterSubject> afterSubject4(new AfterSubject(AfterSubject::Noun{nounPronoun(), std::move(afterNoun3)}));
		uptr<S> s4(new S{false, "", nounPronoun(), "subjECT", std::move(afterSubject4)});
		story.ss.push_back(std::move(s4));

		uptr<AfterObject> afterObject2(new AfterObject(AfterObject::Noun{nounWord1(), "destinatioN", verb(), tenseVerbPastNeg()}));
		uptr<AfterNoun> afterNoun4(new AfterNoun(AfterNoun::Object{"object", std::move(afterObject2)}));
		uptr<AfterSubject> afterSubject5(new AfterSubject(AfterSubject::Noun{nounPronoun(), std::move(afterNoun4)}));
		uptr<S> s5(new S{false, "", nounPronoun(), "subJECT", std::move(afterSubject5)});
		story.ss.push_back(std::move(s5));

		return story;
	}
}

int main()
{
	bool notFailed = true;

	std::ostringstream json;

	sjp::ast::Story story = test::createAst();
	sjp::ast::toJson(json, story);

	std::cout << json.str();

	notFailed &= json.str() == R"({"ss":[{"connector":"connector","noun":{"type":"Word1","value":"nounWord1"},"subject":"subject","afterSubject":{"type":"AfterSubjectVerb","verb":{"value":"verb"},"tense":{"type":"Verb","value":"tenseVerb"}}}, {"connector":"connectoR","noun":{"type":"Pronoun","value":"nounPronoun"},"subject":"subjecT","afterSubject":{"type":"AfterSubjectNoun","noun":{"type":"Pronoun","value":"nounPronoun"},"afterNoun":{"type":"AfterNounBe","be":{"type":"Is","value":"beIs"}}}}, {"connector":"connectOR","noun":{"type":"Pronoun","value":"nounPronoun"},"subject":"subjeCT","afterSubject":{"type":"AfterSubjectNoun","noun":{"type":"Pronoun","value":"nounPronoun"},"afterNoun":{"type":"AfterNounDestination","destination":"destination","verb":{"value":"verb"},"tense":{"type":"VerbNeg","value":"tenseVerbNeg"}}}}, {"noun":{"type":"Pronoun","value":"nounPronoun"},"subject":"subjECT","afterSubject":{"type":"AfterSubjectNoun","noun":{"type":"Pronoun","value":"nounPronoun"},"afterNoun":{"type":"AfterNounObject","object":"object","afterObject":{"type":"AfterObjectVerb","verb":{"value":"verb"},"tense":{"type":"VerbPast","value":"tenseVerbPast"}}}}}, {"noun":{"type":"Pronoun","value":"nounPronoun"},"subject":"subJECT","afterSubject":{"type":"AfterSubjectNoun","noun":{"type":"Pronoun","value":"nounPronoun"},"afterNoun":{"type":"AfterNounObject","object":"object","afterObject":{"type":"AfterObjectNoun","noun":{"type":"Word1","value":"nounWord1"},"destination":"destinatioN","verb":{"value":"verb"},"tense":{"type":"VerbPastNeg","value":"tenseVerbPastNeg"}}}}}]})";

	return !notFailed;
}
