#include <token_stream.hpp>

#include <logging.h>

#include <sstream>
#include <string>
#include <iostream>

int main()
{
	using namespace sjp;

	logging::setupLoggingNoFilterClog();

	bool notFailed = true;

	std::istringstream is("test one two three");
	const auto idString = [](const std::string& x){ return x; };

	TokenStream<std::string, decltype(idString), decltype(idString)> ts(is, idString, idString, std::string("ERROR"));

	auto strings = {"test", "one", "two", "three"};
	for (const std::string& s : strings)
	{
		std::cout << "before match" << std::endl;
		ts.matchToken(s);
		std::cout << "after match" << std::endl;
		notFailed &= ts.matchedString() == s;
	}

	return !notFailed;
}
