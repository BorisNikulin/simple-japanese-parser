#include <shape.hpp>

int main()
{
	bool notFailed = true;

	sjp::vector::Shape<10,5> shape_10_5;
	sjp::vector::Shape<4,3,2> shape_4_3_2;

	notFailed &= sjp::vector::Shape<42>::size == 42;

	notFailed &= shape_10_5.size == 10 * 5;
	notFailed &= shape_10_5(0,0) == 0;
	notFailed &= shape_10_5(0,1) == 1;
	notFailed &= shape_10_5(1,0) == 5;
	notFailed &= shape_10_5(1,1) == 6;
	notFailed &= shape_10_5(3,3) == 18;

	notFailed &= shape_4_3_2.size == 4 * 3 * 2;
	notFailed &= shape_4_3_2(0,0,0) == 0;
	notFailed &= shape_4_3_2(0,0,1) == 1;
	notFailed &= shape_4_3_2(0,1,0) == 2;
	notFailed &= shape_4_3_2(1,0,0) == 6;
	notFailed &= shape_4_3_2(1,1,1) == 6 + 2 + 1;
	notFailed &= shape_4_3_2(3,2,1) == 3 * 6 + 2 * 2 + 1;

	return !notFailed;
}
