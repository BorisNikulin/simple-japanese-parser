#include <token_stream.hpp>

#include <logging.h>

#include <initializer_list>
#include <sstream>
#include <string>

namespace test {
	enum class Token
	{
		LOWER_CASE, UPPER_CASE, NEITHER_CHAR, STRING, ERROR
	};

	std::string tokenToString(const Token token)
	{
		return std::to_string(static_cast<int>(token));
	}

	Token scan(const std::string& str)
	{
		if (str.length() == 1)
		{
			if (str[0] < 'A')
			{
				return Token::NEITHER_CHAR;
			}
			else if (str[0] <= 'Z')
			{
				return Token::UPPER_CASE;
			}
			else if (str[0] <= 'z')
			{
				return Token::LOWER_CASE;
			}
			else
			{
				return Token::NEITHER_CHAR;
			}
		}
		else
		{
			return Token::STRING;
		}
	}
}

int main()
{
	using namespace sjp;

	logging::setupLoggingNoFilterClog();

	bool notFailed = true;

	std::istringstream is("! a b z A B Z { str");

	TokenStream<test::Token> ts(is, test::scan, test::tokenToString, test::Token::ERROR);

	std::initializer_list<std::pair<std::string, test::Token>> stringsAndTokens =
		{
			{"!", test::Token::NEITHER_CHAR},
			{"a", test::Token::LOWER_CASE},
			{"b", test::Token::LOWER_CASE},
			{"z", test::Token::LOWER_CASE},
			{"A", test::Token::UPPER_CASE},
			{"B", test::Token::UPPER_CASE},
			{"Z", test::Token::UPPER_CASE},
			{"{", test::Token::NEITHER_CHAR},
			{"str", test::Token::STRING},
		};

	for (const auto& stringAndToken : stringsAndTokens)
	{
		ts.matchToken(stringAndToken.second);
		notFailed &= ts.matchedString() == stringAndToken.first;
	}

	return !notFailed;
}
