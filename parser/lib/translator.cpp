#include "translator.h"

#include "ast.h"
#include "parser.h"

#include <ostream>
#include <iomanip>
#include <unordered_map>
#include <utility>
#include <string>

namespace sjp {
	void translate(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts){
		translator::translateStory(out, dictionary, ts); // entry for translating a story
	};

	void translate(std::ostream& out, const Dictionary& dictionary, const ast::Story& story)
	{
		translator::translateStory(out, dictionary, story); // entry for translation without parsing
	}

	Dictionary parseDictionry(std::istream& in)
	{
		Dictionary dictionary;
		std::pair<std::string, std::string> dictEntry;

		while (in)
		{
			in >> dictEntry.first;
			in >> dictEntry.second;
			dictionary.insert(dictEntry);
		}

		return dictionary;
	}

	namespace translator {
		std::string getEword(const Dictionary& dictionary, const std::string& japaneseWord)
		{
			const auto& it = dictionary.find(japaneseWord);
			if (it != dictionary.cend())
			{
				return it->second;
			}
			else
			{
				return japaneseWord;
			}
		}

		void gen(std::ostream& out, GEN_TYPE genType, const std::string& value)
		{
			const int labelWidth = 14;
			// translate type to an actual connector string
			static const std::unordered_map<GEN_TYPE, std::string> genTypeToLabels =
				{
					{GEN_TYPE::CONNECTOR, "CONNECTOR:"},
					{GEN_TYPE::ACTOR, "ACTOR:"},
					{GEN_TYPE::DESCRIPTION, "DESCRIPTION:"},
					{GEN_TYPE::TENSE, "TENSE:"},
					{GEN_TYPE::OBJECT, "OBJECT:"},
					{GEN_TYPE::TO, "TO:"},
					{GEN_TYPE::ACTION, "ACTION:"}
				};

			out << std::left << std::setw(labelWidth) << genTypeToLabels.at(genType) << value << "\n";
		}

		void translateAfterNoun(
				std::ostream& out,
				const Dictionary& dictionary,
				TokenStream<Token>& ts,
				const std::string& lastEWord
			)
		{
			logTrace("Processing <afterNoun>");

			// get next token or throw if not in list
			switch (ts.nextToken({Token::IS, Token::WAS, Token::DESTINATION, Token::OBJECT}))
			{
				case Token::IS: // Translation for <be> begging
				case Token::WAS:
				{
					std::unique_ptr<ast::Be> be = parser::parseBe(ts); // parse be

					gen(out, GEN_TYPE::DESCRIPTION, lastEWord); // translation for description

					gen(out, GEN_TYPE::TENSE, ast::beTypeToString(be->type)); // translation for tense

					ts.matchToken(Token::PERIOD); // match period
					break;
				}
				case Token::DESTINATION: // translation for DESTINATION beginning
				{
					ts.matchToken(Token::DESTINATION); // match DESTINATION

					gen(out, GEN_TYPE::TO, lastEWord); // translate to

					std::unique_ptr<ast::Verb> verb = parser::parseVerb(ts); // parsing for verb
					gen(out, GEN_TYPE::ACTION, getEword(dictionary, verb->value)); // translation for verb

					std::unique_ptr<ast::Tense> tense = parser::parseTense(ts); // parsing for tense
					gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(tense->type)); // translation for tense

					ts.matchToken(Token::PERIOD); // match period
					break;
				}
				case Token::OBJECT:
				{
					ts.matchToken(Token::OBJECT); // match object

					gen(out, GEN_TYPE::OBJECT, lastEWord); // parsing for object

					translateAfterObject(out, dictionary, ts); // the rest of translation after object
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;

			}
		}
		void translateAfterSubject(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts)
		{
			logTrace("Processing <afterSubject>");

			switch (ts.nextToken({Token::WORD2, Token::WORD1, Token::PRONOUN})) // get next token or throw if not in list
			{
				case Token::WORD2: // WORD2 is next token
				{
					std::unique_ptr<ast::Verb> verb = parser::parseVerb(ts); // parsing for verb
					gen(out, GEN_TYPE::ACTION, getEword(dictionary, verb->value)); // translation for verb

					std::unique_ptr<ast::Tense> tense = parser::parseTense(ts); // parsing for tense
					gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(tense->type)); // translation for tense

					ts.matchToken(Token::PERIOD); // Match period or throw
					break;
				}
				case Token::WORD1: // WORD1 Or PRONOUN are next tokens, which is a noun
				case Token::PRONOUN:
				{
					std::unique_ptr<ast::Noun> verb = parser::parseNoun(ts);
					translateAfterNoun(out, dictionary,  ts, getEword(dictionary, ts.matchedString()));
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
		}

		void translateAfterObject(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts)
		{
			logTrace("Processing <afterObject>");
			switch (ts.nextToken({Token::WORD2, Token::WORD1, Token::PRONOUN})) // get next token or throw if not in list
			{
				case Token::WORD2: // WORD2 is next token
				{
					std::unique_ptr<ast::Verb> verb = parser::parseVerb(ts); // get the verb pointer
					gen(out, GEN_TYPE::ACTION, getEword(dictionary, verb->value)); // translate the verb

					std::unique_ptr<ast::Tense> tense = parser::parseTense(ts); // get the tense pointer
					gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(tense->type)); // translate the tense

					ts.matchToken(Token::PERIOD); // Match period or throw
					break;
				}
				case Token::WORD1: // WORD1 Or PRONOUN are next tokens, which is a noun
				case Token::PRONOUN:
				{
					std::unique_ptr<ast::Noun> noun = parser::parseNoun(ts); // get noun pointer
					ts.matchToken(Token::DESTINATION); // Match DESTINATION
					gen(out, GEN_TYPE::TO, getEword(dictionary, noun->value)); // translate TO

					std::unique_ptr<ast::Verb> verb = parser::parseVerb(ts); // get the verb pointer
					gen(out, GEN_TYPE::ACTION, getEword(dictionary, verb->value)); // translate ACTION

					std::unique_ptr<ast::Tense> tense = parser::parseTense(ts); // get tense pointer
					gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(tense->type)); // translate TENSE
					ts.matchToken(Token::PERIOD); // Match period or throw
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
		}

		void translateS(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts) {
			if (ts.nextToken() == Token::CONNECTOR)
			{
				ts.matchToken(Token::CONNECTOR);
				gen(out, GEN_TYPE::CONNECTOR, getEword(dictionary, ts.matchedString()));
			}

			std::unique_ptr<ast::Noun> noun = parser::parseNoun(ts);
			ts.matchToken(Token::SUBJECT);
			gen(out, GEN_TYPE::ACTOR, noun->value);

			translateAfterSubject(out, dictionary, ts);
		}

		void translateStory(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts) {
			logTrace("Processing <story>");
			do
			{
				translateS(out, dictionary, ts);
				out << "\n";
			} while (ts.nextToken() != Token::EOFM); // check for EOFM token and continue if it isnt found
			ts.matchToken(Token::EOFM); // Match EOFM token as it has to exist at the end
		}



		// AST translation functions

		void translateAfterObjectVerb(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterObject::Verb& afterObjectVerb
			)
		{
			gen(out, GEN_TYPE::ACTION, getEword(dictionary, afterObjectVerb.verb->value));
			gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(afterObjectVerb.tense->type));
		}

		void translateAfterObjectNoun(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterObject::Noun& afterObjectNoun
			)
		{
			gen(out, GEN_TYPE::TO, getEword(dictionary, afterObjectNoun.noun->value));
			gen(out, GEN_TYPE::ACTION, getEword(dictionary, afterObjectNoun.verb->value));
			gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(afterObjectNoun.tense->type));
		}

		void translateAfterObject(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterObject& afterObject
			)
		{
			switch (afterObject.type)
			{
				case ast::AfterObject::Type::VERB:
					translateAfterObjectVerb(out, dictionary, afterObject.verb);
					break;
				case ast::AfterObject::Type::NOUN:
					translateAfterObjectNoun(out, dictionary, afterObject.noun);
					break;
			}
		}

		void translateAfterNounBe(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterNoun::Be& afterNounBe,
				const std::string& lastEWord
			)
		{
			gen(out, GEN_TYPE::DESCRIPTION, lastEWord);
			gen(out, GEN_TYPE::TENSE, ast::beTypeToString(afterNounBe.be->type));
		}

		void translateAfterNounDestination(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterNoun::Destination& afterNounDestination,
				const std::string& lastEWord
			)
		{
			gen(out, GEN_TYPE::TO, lastEWord);
			gen(out, GEN_TYPE::ACTION, getEword(dictionary, afterNounDestination.verb->value));
			gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(afterNounDestination.tense->type));
		}

		void translateAfterNounObject(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterNoun::Object& afterNounObject,
				const std::string& lastEWord
			)
		{
			gen(out, GEN_TYPE::OBJECT, lastEWord);

			translateAfterObject(out, dictionary, *afterNounObject.afterObject);
		}

		void translateAfterNoun(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterNoun& afterNoun,
				const std::string& lastEWord
			)
		{
			switch (afterNoun.type)
			{
				case ast::AfterNoun::Type::BE:
					translateAfterNounBe(out, dictionary, afterNoun.be, lastEWord);
					break;
				case ast::AfterNoun::Type::DESTINATION:
					translateAfterNounDestination(out, dictionary, afterNoun.destination, lastEWord);
					break;
				case ast::AfterNoun::Type::OBJECT:
					translateAfterNounObject(out, dictionary, afterNoun.object, lastEWord);
					break;
			}
		}

		void translateAfterSubjectVerb(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterSubject::Verb& afterSubjectVerb
			)
		{
			gen(out, GEN_TYPE::ACTION, getEword(dictionary, afterSubjectVerb.verb->value));
			gen(out, GEN_TYPE::TENSE, ast::tenseTypeToString(afterSubjectVerb.tense->type));
		}

		void translateAfterSubjectNoun(
				std::ostream& out,
				const Dictionary& dictionary,
				const ast::AfterSubject::Noun& afterSubjectNoun
			)
		{
			translateAfterNoun(out, dictionary, *afterSubjectNoun.afterNoun, getEword(dictionary, afterSubjectNoun.noun->value));
		}

		void translateAfterSubject(std::ostream& out, const Dictionary& dictionary, const ast::AfterSubject& afterSubject)
		{
			switch (afterSubject.type)
			{
				case ast::AfterSubject::Type::VERB:
					translateAfterSubjectVerb(out, dictionary, afterSubject.verb);
					break;
				case ast::AfterSubject::Type::NOUN:
					translateAfterSubjectNoun(out, dictionary, afterSubject.noun);
					break;
			}
		}

		void translateS(std::ostream& out, const Dictionary& dictionary, const ast::S& s)
		{
			if (s.hasConnector)
			{
				gen(out, GEN_TYPE::CONNECTOR, getEword(dictionary, s.connector));
			}

			gen(out, GEN_TYPE::ACTOR, s.noun->value);

			translateAfterSubject(out, dictionary, *s.afterSubject);
		}

		void translateStory(std::ostream& out, const Dictionary& dictionary, const ast::Story& story)
		{
			for (const auto& s : story.ss)
			{
				translateS(out, dictionary, *s);
				out << "\n";
			}
		}
	}
}
