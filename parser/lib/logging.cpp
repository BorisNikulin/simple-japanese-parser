#include "logging.h"

#include <iostream>

namespace sjp {
	namespace logging {
		LogingConfig loggingConfig;

		void setupLoggingNoFilterClog()
		{
			loggingConfig.filter = [](const sjp::Severity){ return true; };
			loggingConfig.render = [](const sjp::Severity, const std::string& msg){ std::clog << msg << "\n"; };
		}
	}

	void log(const Severity severity, const std::string& msg)
	{
		if (logging::loggingConfig.filter(severity))
		{
			logging::loggingConfig.render(severity, msg);
		}
	}

	void logError(const std::string& msg)
	{
		log(Severity::ERROR, msg);
	}

	void logWarn(const std::string& msg)
	{
		log(Severity::WARN, msg);
	}

	void logInfo(const std::string& msg)
	{
		log(Severity::INFO, msg);
	}

	void logTrace(const std::string& msg)
	{
		log(Severity::TRACE, msg);
	}

}
