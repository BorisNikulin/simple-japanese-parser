#include "ast.h"

#include <algorithm>
#include <ostream>

namespace sjp { namespace ast {
	std::string beTypeToString(Be::Type type)
	{
		switch (type)
		{
			case ast::Be::Type::IS:
				return "IS";
			case ast::Be::Type::WAS:
				return "WAS";
			default:
				throw;
		}
	}

	std::string tenseTypeToString(Tense::Type  type)
	{
		switch (type)
		{
			case ast::Tense::Type::VERB:
				return "VERB";
			case ast::Tense::Type::VERB_NEG:
				return "VERB_NEG";
			case ast::Tense::Type::VERB_PAST:
				return "VERB_PAST";
			case ast::Tense::Type::VERB_PAST_NEG:
				return "VERB_PAST_NEG";
			default:
				throw;
		}
	}

	void toJson(std::ostream& os, const Story& story)
	{
		os << "{\"ss\":[";

		if (!story.ss.empty())
		{
			toJson(os, *story.ss.front());

			std::for_each(
				++story.ss.begin(),
				story.ss.end(),
				[&os](const std::unique_ptr<ast::S>& s){ os << ", "; toJson(os, *s); }
			);
		}

		os << "]}";
	}

	void toJson(std::ostream& os, const S& s)
	{
		os << "{";

		if (s.hasConnector)
		{
			os << "\"connector\":\"" << s.connector << "\",";
		}

		os << "\"noun\":";
		toJson(os, *s.noun);

		os << ",\"subject\":\"" << s.subject << "\"";

		os << ",\"afterSubject\":";
		toJson(os, *s.afterSubject);

		os << "}";
	};

	void toJson(std::ostream& os, const AfterSubject::Verb& afterSubjectVerb)
	{
		os << "{";

		os << "\"type\":\"AfterSubjectVerb\"";

		os << ",\"verb\":";
		toJson(os, *afterSubjectVerb.verb);

		os << ",\"tense\":";
		toJson(os, *afterSubjectVerb.tense);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterSubject::Noun& afterSubjectNoun)
	{
		os << "{";

		os << "\"type\":\"AfterSubjectNoun\"";

		os << ",\"noun\":";
		toJson(os, *afterSubjectNoun.noun);

		os << ",\"afterNoun\":";
		toJson(os, *afterSubjectNoun.afterNoun);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterSubject& afterSubject)
	{
		switch (afterSubject.type)
		{
			case AfterSubject::Type::VERB:
				toJson(os, afterSubject.verb);
				break;
			case AfterSubject::Type::NOUN:
				toJson(os, afterSubject.noun);
				break;
		}
	}

	void toJson(std::ostream& os, const AfterNoun::Be& afterNounBe)
	{
		os << "{";

		os << "\"type\":\"AfterNounBe\"";

		os << ",\"be\":";
		toJson(os, *afterNounBe.be);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterNoun::Destination& afterNounDestination)
	{
		os << "{";

		os << "\"type\":\"AfterNounDestination\"";

		os << ",\"destination\":\"" << afterNounDestination.destination << "\"";

		os << ",\"verb\":";
		toJson(os, *afterNounDestination.verb);

		os << ",\"tense\":";
		toJson(os, *afterNounDestination.tense);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterNoun::Object& afterNounObject)
	{

		os << "{";

		os << "\"type\":\"AfterNounObject\"";

		os << ",\"object\":\"" << afterNounObject.object << "\"";

		os << ",\"afterObject\":";
		toJson(os, *afterNounObject.afterObject);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterNoun& afterNoun)
	{
		switch (afterNoun.type)
		{
			case AfterNoun::Type::BE:
				toJson(os, afterNoun.be);
				break;
			case AfterNoun::Type::DESTINATION:
				toJson(os, afterNoun.destination);
				break;
			case AfterNoun::Type::OBJECT:
				toJson(os, afterNoun.object);
				break;
		}
	}

	void toJson(std::ostream& os, const AfterObject::Verb& afterObjectVerb)
	{
		os << "{";

		os << "\"type\":\"AfterObjectVerb\"";

		os << ",\"verb\":";
		toJson(os, *afterObjectVerb.verb);

		os << ",\"tense\":";
		toJson(os, *afterObjectVerb.tense);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterObject::Noun& afterObjectNoun)
	{
		os << "{";

		os << "\"type\":\"AfterObjectNoun\"";

		os << ",\"noun\":";
		toJson(os, *afterObjectNoun.noun);

		os << ",\"destination\":\"" << afterObjectNoun.destination << "\"";

		os << ",\"verb\":";
		toJson(os, *afterObjectNoun.verb);

		os << ",\"tense\":";
		toJson(os, *afterObjectNoun.tense);

		os << "}";
	}

	void toJson(std::ostream& os, const AfterObject& afterObject)
	{
		switch (afterObject.type)
		{
			case AfterObject::Type::VERB:
				toJson(os, afterObject.verb);
				break;
			case AfterObject::Type::NOUN:
				toJson(os, afterObject.noun);
				break;
		}
	}

	void toJson(std::ostream& os, const Noun& noun)
	{
		os << "{";

		os << "\"type\":\"";
		switch (noun.type)
		{
			case Noun::Type::WORD1:
				os << "Word1";
				break;
			case Noun::Type::PRONOUN:
				os << "Pronoun";
				break;
		}
		os << "\"";

		os << ",\"value\":\"" << noun.value << "\"";

		os << "}";
	}

	void toJson(std::ostream& os, const Verb& verb)
	{
		os << "{";

		os << "\"value\":\"" << verb.value << "\"";

		os << "}";
	}

	void toJson(std::ostream& os, const Be& be)
	{
		os << "{";

		os << "\"type\":\"";
		switch (be.type)
		{
			case Be::Type::IS:
				os << "Is";
				break;
			case Be::Type::WAS:
				os << "Was";
				break;
		}
		os << "\"";

		os << ",\"value\":\"" << be.value << "\"";

		os << "}";
	}

	void toJson(std::ostream& os, const Tense& tense)
	{
		os << "{";

		os << "\"type\":\"";
		switch (tense.type)
		{
			case Tense::Type::VERB:
				os << "Verb";
				break;
			case Tense::Type::VERB_NEG:
				os << "VerbNeg";
				break;
			case Tense::Type::VERB_PAST:
				os << "VerbPast";
				break;
			case Tense::Type::VERB_PAST_NEG:
				os << "VerbPastNeg";
				break;
		}
		os << "\"";

		os << ",\"value\":\"" << tense.value << "\"";

		os << "}";
	}
} }
