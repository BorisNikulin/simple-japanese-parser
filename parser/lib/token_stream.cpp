#include "token_stream.hpp"

#include <sstream>
#include <set>

namespace sjp {
	template<>
	std::string TokenMismatch<Token>::constructWhatString(
		const Token& expectedToken,
		const Token& actualToken,
		const std::string& actualString
	)
	{
		std::ostringstream what;

		what << "SYNTAX ERROR: expected "
			<< tokenToString(expectedToken)
			<< " but found "
			<<  '"' << actualString << '"'
			<< " of type "
			<< tokenToString(actualToken);

		return what.str();
	}

	template<>
	std::string TokenUnexpected<Token>::constructWhatString(
		const std::set<Token>& expectedTokens,
		const Token& actualToken,
		const std::string& actualString
	)
	{
		std::ostringstream what;

		what << "SYNTAX ERROR: unexpected "
			<<  '"' << actualString << '"'
			<< " of type "
			<< tokenToString(actualToken)
			<< " when expecting one of ";


	auto it = expectedTokens.begin();

	if (it != expectedTokens.end())
	{
		what << tokenToString(*it);
		++it;

		for (; it != expectedTokens.end(); ++it)
		{
			what << ", "
				<< tokenToString(*it);
		}
	}

		return what.str();
	}

	template<>
	std::string LexicalError<Token>::constructWhatString(const std::string& invalidString)
	{

		std::ostringstream what;

		what << "LEXICAL ERROR: "
			<< '"' << invalidString << '"'
			<< " is not a valid token";

		return what.str();
	}
}
