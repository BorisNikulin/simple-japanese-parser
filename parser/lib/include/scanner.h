#pragma once

#include "token.h"

#include <istream>
#include <string>
#include <utility>

namespace sjp
{
	/** Extracts a string from the filestream and scans it
	 *  with scan(const std::string&).
	 *
	 *  Scans one std::string delimited by spaces from the input stream
	 *  and tokenizes it according to the specification of the simple subset of Japanese roamji.
	 *
	 *  \param in stream of characters that contains words deliminated by whitespace
	 *
	 *  \returns a pair of the \ref Token of the scanned in word
	 *  	and the word itself
	 *
	 *  \see scan(const std::string&).
	 */
	std::pair<Token, std::string> scan(std::istream& in);

	/** Tokenize given word.
	 *
	 *  Tokenizes given word according to
	 *  the specification of the simple subset of Japanese roamji.
	 *
	 *  \pre
	 *  	The given word must be trimmed of whitespace
	 *  	on both ends.
	 *
	 *  \param word word to tokenize trimmed of whitespace
	 *
	 *  \returns a pair of the \ref Token of the scanned in word
	 *  	and the word itself
	 *
	 *  \see
	 *  	scan(std::istream& in),
	 *  	tokenizeWord(),
	 *  	tokenizePeriod(),
	 *  	tokenizeReservedWord().
	 */
	std::pair<Token, std::string> scan(const std::string& word);

	namespace scanner
	{
		/** Tokenize word as a period.
		 *
		 *  \returns Token::PERIOD if the word is a period
		 *  	or Token::ERROR if not
		 */
		Token tokenizePeriod(const std::string& word);

		/** Tokenize word as a reservered word.
		 *
		 *  \returns \ref Token as type of token
		 *  	or Token::ERROR if not a reservered word
		 */
		Token tokenizeReservedWord(const std::string& word);

		/** Tokenize word as a Token::WORD1 or Token::WORD2.
		 *
		 *  Checks if a given word with no whitespace
		 *  is a sentence of as simple Japanese romaji subset regulkar expression.
		 *
		 *  RE: (vowel | vowel n | consonant vowel | consonant vowel n |
		 *  consonant-pair vowel | consonant-pair vowel n)^+
		 *
		 *  All letters are lower case except for I and E which are a part of vowel.
		 *
		 *  If a valid word ends in I or E then it is a Token::WORD2,
		 *  otherwise it is a Token::WORD1.
		 *
		 *  \returns \ref Token as a type of token
		 *  	or Token::ERROR if not a valid word token
		 */
		Token tokenizeWord(const std::string& word);
	}
}
