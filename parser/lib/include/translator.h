#pragma once

#include <token.h>
#include <token_stream.hpp>
#include <ast.h>

#include <string>
#include <ostream>
#include <istream>
#include <unordered_map>
#include <cstddef>
#include <functional>

namespace sjp {
	using Dictionary = std::unordered_map<std::string, std::string>;

	/** Parse and translate a token stream to specified output stream using given dictionary.
	 */
	void translate(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts);

	/** Translate an AST using the given dictionary to the specified output stream.
	 */
	void translate(std::ostream& out, const Dictionary& dictionary, const ast::Story& story);

	/** Create a dictionry for use with tramslator::getEword() from an input stream.
	 */
	Dictionary parseDictionry(std::istream& in);

	namespace translator {
		/** Tranlate a Japanese word to English using the provided dictionary.
		 *
		 *  \attention
		 *  	If there is no translation for the word,
		 *  	the word returned will be the same Japanese word.
		 *
		 *  \param dictionary the Japansese to English dictionary
		 *  \param japaneseWord word to attempt to translatet to English
		 *
		 *  \return The English translation or the given word if not possible
		 */
		std::string getEword(const Dictionary& dictionary, const std::string& japaneseWord);

		enum class GEN_TYPE : short
		{
			CONNECTOR, ACTOR, ACTION, TENSE, DESCRIPTION, OBJECT, TO
		};

		/** Generate a specified type of IR using provided data an output destination.
		 *
		 *  \param out output stream to write IR to
		 *  \param genType type of IR to generate
		 *  \param value data to use for IR generation
		 */
		void gen(std::ostream& out, GEN_TYPE genType, const std::string& value);

		void translateStory(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts);
		void translateS(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts);
		void translateAfterObject(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts);
		void translateAfterSubject(std::ostream& out, const Dictionary& dictionary, TokenStream<Token>& ts);
		void translateAfterNoun(std::ostream& out,const Dictionary& dictionary,TokenStream<Token>& ts,const std::string& lastEWord);

		void translateStory(std::ostream& out, const Dictionary& dictionary, const ast::Story& story);
		void translateS(std::ostream& out, const Dictionary& dictionary, const ast::S& s);
		void translateAfterSubject(std::ostream& out, const Dictionary& dictionary, const ast::AfterSubject& afterSubject);
		void translateAfterNoun(std::ostream& out, const Dictionary& dictionary, const ast::AfterNoun& afterNoun, const std::string& lastEWord);
		void translateAfterObject(std::ostream& out, const Dictionary& dictionary, const ast::AfterObject& afterObject);
	}
}

namespace std {
	// Enum classes are not hashable pre c++14 (compiler defect) (C++11 is great)
	template <>
	struct hash<sjp::translator::GEN_TYPE>
	{
		std::size_t operator()(const sjp::translator::GEN_TYPE& genType) const noexcept
		{
			return std::hash<short>{}(static_cast<short>(genType));
		}
	};
}
