#pragma once

#include "logging.h"
#include "scanner.h"

#include <istream>
#include <stdexcept>
#include <string>
#include <utility>
#include <set>

namespace sjp {
	/** Base class of all TokenStreamErrors.
	 *
	 *  \see TokenMismatch, TokenUnexpected, LexicalError.
	 */
	class TokenStreamError : public std::runtime_error
	{
		public:
			TokenStreamError(const char* what) : std::runtime_error(what)
			{}
			TokenStreamError(const std::string& what) : std::runtime_error(what)
			{}

	};

	/** Error representing a failed token match.
	 *
	 *  Contains expected token passed to TokenStream::matchToken()
	 *  as well as the actual token and string.
	 *
	 *  Error message in the inherited TokenMismatch::what()
	 *  can be customized for certain token types
	 *  via template specialization
	 *  of constructWhatString().
	 *
	 *  \tparam TokenType the type of token
	 *  	and token type of the TokenStream throwing the exception
	 */
	template<typename TokenType>
	class TokenMismatch : public TokenStreamError
	{
		private:
			/** Construct the string retured by the inherited
			 *  TokenMismatch::what().
			 */
			std::string constructWhatString(
				const TokenType& expectedToken,
				const TokenType& actualToken,
				const std::string& actualString
			)
			{
				return "TokenMismatch default error message";
			}

		public:
			const TokenType expectedToken;
			const TokenType actualToken;
			const std::string actualString;

			TokenMismatch(/* position */
					const TokenType& expectedToken,
					const TokenType& actualToken,
					const std::string& actualString
				) :
					expectedToken(expectedToken),
					actualToken(actualToken),
					actualString(actualString),
					TokenStreamError(constructWhatString(expectedToken, actualToken, actualString))
		{}
	};

	/** Error representing a failed look ahead,
	 *
	 *  Contains expected tokens passed to TokenStream::matchToken(std::set&)
	 *  as well as the actual next token and string.
	 *
	 *  Error message in the inherited TokenUnexpected::what()
	 *  can be customized for certain token types
	 *  via template specialization
	 *  of constructWhatString().
	 *
	 *  \tparam TokenType the type of token
	 *  	and token type of the TokenStream throwing the exception
	 */
	template<typename TokenType>
	class TokenUnexpected : public TokenStreamError
	{
		private:
			/** Construct the string retured by the inherited
			 *  TokenUnexpected::what().
			 */
			std::string constructWhatString(
				const std::set<TokenType>& expectedToken,
				const TokenType& actualToken,
				const std::string& actualString
			)
			{
				return "TokenUnexpected default error message";
			}

		public:
			const std::set<TokenType> expectedTokens;
			const TokenType actualToken;
			const std::string actualString;

			TokenUnexpected(/* position */
					const std::set<TokenType>& expectedTokens,
					const TokenType& actualToken,
					const std::string& actualString
				) :
					expectedTokens(expectedTokens),
					actualToken(actualToken),
					actualString(actualString),
					TokenStreamError(constructWhatString(expectedTokens, actualToken, actualString))
		{}
	};

	template<typename TokenType>
	class LexicalError : public TokenStreamError
	{
		private:
			const std::string invalidString;

			std::string constructWhatString(const std::string& invalidString)
			{
				return "LexicalError default error message";
			}

		public:
			LexicalError(const std::string& invalidString)
				: invalidString(invalidString),
					TokenStreamError(constructWhatString(invalidString))
			{}
	};

	/** Wrapper around a std::istream and scanner function
	 *  for reading TokenTypes
	 *  and matching them with look ahead capability.
	 *
	 *  Given a std::istream and a function from std::string to TokenType.
	 *  one can use matchToken() to expect a specific TokenType
	 *  and advance the stream.
	 *  nextToken() can provide look ahead.
	 *
	 *  \pre Requires logging::loggingConfig to be configured or segfaults can happen.
	 *
	 *  \tparam TokenType type of tokens processed by the stream
	 *  \tparam ScannerFunc type of the scanner function used to convert
	 *  	std::string to TokenType
	 */
	template<
		typename TokenType,
		typename ScannerFunc = TokenType (&)(const std::string&),
		typename TokenToStringFunc = std::string (&)(const TokenType)
	>
	class TokenStream
	{
		private:
			std::istream& is;
			const ScannerFunc& scanner;
			const TokenToStringFunc& tokenToString;
			const TokenType errorToken;

			TokenType currentToken;
			std::string currentString;

			bool haveNextToken = false;
			TokenType _nextToken;
			std::string nextString;

			void checkToken(const TokenType& token, const std::string& str);
			void advance();
			void lookAhead();

		public:
			/** Constructs a TokenStream using the underlying std::istream
			 *  to extract strings from and convert to a token of type TokenType.
			 *
			 *  \attention
			 *  	The lifetimes of both arguments must excede
			 *  	this object's lifetime.
			 *
			 *  \param is input stream of characters
			 *  \param scanner function converting std::string to TokenType
			 *  \param tokenToString function converting TokenType to a string
			 *  \param errorToken token that if returned from scanner
			 *  	is considered an error and will raise a LexicalError
			 */
			TokenStream(
					std::istream& is,
					const ScannerFunc& scanner,
					const TokenToStringFunc& tokenToString,
					const TokenType& errorToken
				) :
					is(is),
					scanner(scanner),
					tokenToString(tokenToString),
					errorToken(errorToken)
			{}

			/** Expect the next token to be as provided
			 *  or throw TokenMismatch.
			 *
			 *  Also advances the stream.
			 *  The string used by the scanner tokenize
			 *  can be accessed by matchedString().
			 *
			 *  \pre
			 *  	The underlying stream should not be empty.
			 *
			 *  \attention
			 *  	Calling this function when the underlying stream is empty
			 *  	is undefined behaviour,
			 *  	likely an error propagating from the underlying stream.
			 *
			 *  \param token the token to expect
			 *
			 *  \throws TokenMismatch
			 *
			 *  \see matchedString()
			 */
			void matchToken(TokenType token);

			/** Get the string associated with just matched token.
			 *
			 *  \pre
			 *  	matchToken() must be called before this function.
			 *
			 *  \attention
			 *  	calling this function before matcheToken() is undefined behaviour.
			 *
			 *  \see matchToken()
			 */
			const std::string& matchedString()
			{ return currentString; }

			/** Allows looking ahead to the next token without advancing the stream.
			 *
			 *  Useful for looking ahead for specific tokens.
			 *
			 *  \param expectedTokens the tokens that a look ahead must contain
			 *  	otherwise throw a TokenUnexpected error
			 *
			 *  \pre
			 *  	The underlying stream should not be empty.
			 *
			 *  \attention
			 *  	Calling this function when the underlying stream is empty
			 *  	is undefined behaviour,
			 *  	likely an error propagating from the underlying stream.
			 */
			TokenType nextToken(const std::set<TokenType>& expectedTokens);

			/** Allows looking ahead to the next token without advancing the stream.
			 *
			 *  Useful for looking ahead for optional tokens.
			 *
			 *  \pre
			 *  	The underlying stream should not be empty.
			 *
			 *  \attention
			 *  	Calling this function when the underlying stream is empty
			 *  	is undefined behaviour,
			 *  	likely an error propagating from the underlying stream.
			 */
			TokenType nextToken();
	};


	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	void TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::checkToken(const TokenType& token, const std::string& str)
	{
		if (token == errorToken)
		{
			throw LexicalError<TokenType>(str);
		}
	}

	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	void TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::advance()
	{
		if (haveNextToken)
		{
			currentString = std::move(nextString);
			currentToken = _nextToken;
		}
		else
		{
			is >> currentString;
			currentToken = scanner(currentString);

			checkToken(currentToken, currentString);
		}

		haveNextToken = false;
	}

	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	void TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::lookAhead()
	{
		if (!haveNextToken)
		{
			haveNextToken = true;
			is >> nextString;
			_nextToken = scanner(nextString);

			checkToken(_nextToken, nextString);
		}
	}

	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	void TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::matchToken(TokenType expectedToken)
	{
		advance();


		if (expectedToken != currentToken)
		{
			throw TokenMismatch<TokenType>(/* position, */ expectedToken, currentToken, currentString);
		}

		logTrace("Matched " + tokenToString(expectedToken));
	}

	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	TokenType TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::nextToken(const std::set<TokenType>& expectedTokens)
	{
		lookAhead();

		auto it = expectedTokens.find(_nextToken);
		if (it == expectedTokens.end())
		{
			throw TokenUnexpected<TokenType>(expectedTokens, _nextToken, nextString);
		}

		return _nextToken;
	}

	template<typename TokenType, typename ScannerFunc, typename TokenToStringFunc>
	TokenType TokenStream<TokenType, ScannerFunc, TokenToStringFunc>::nextToken()
	{
		lookAhead();

		return _nextToken;
	}
}
