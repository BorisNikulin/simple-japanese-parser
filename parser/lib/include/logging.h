#pragma once

#include <string>

namespace sjp {
	enum class Severity : short
	{
		ERROR, WARN, INFO, TRACE
	};

	void log(const Severity, const std::string& msg);
	void logError(const std::string& msg);
	void logWarn(const std::string& msg);
	void logInfo(const std::string& msg);
	void logTrace(const std::string& msg);

	namespace logging {
		extern struct LogingConfig
		{
			bool (*filter)(const Severity);
			void (*render)(const Severity, const std::string& msg);
		} loggingConfig;

		void setupLoggingNoFilterClog();
	}

}
