#pragma once

#include <string>

namespace sjp {
	/** Token type of a word.
	 *
	 *  \ref Token represents either specific reservered words
	 *  matching a specific regular expression
	 *  or reserved words.
	 *
	 *  \see scan()
	 */
	enum class Token
	{
		/// General word not ending in I or E.
		WORD1,
		/// General word ending in I or E.
		WORD2,

		PERIOD,

		VERB,
		VERB_NEG,
		VERB_PAST,
		VERB_PAST_NEG,

		IS,
		WAS,

		OBJECT,
		SUBJECT,
		DESTINATION,

		PRONOUN,
		CONNECTOR,

		EOFM,

		ERROR
	};

	/** Convert a Token to a std::string.
	 */
	std::string tokenToString(const Token token);
}
