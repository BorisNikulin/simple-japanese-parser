#pragma once

#include <deque>
#include <memory>
#include <string>
#include <utility>
#include <ostream>

namespace sjp { namespace ast {

	struct Noun
	{
		enum Type : bool { WORD1, PRONOUN };

		Type type;
		std::string value;
	};

	struct Verb
	{
		std::string value;
	};

	struct Be
	{
		enum Type : bool { IS, WAS };

		Type type;
		std::string value;
	};

	std::string beTypeToString(Be::Type  type);

	struct Tense
	{
		enum Type : short { VERB, VERB_NEG, VERB_PAST, VERB_PAST_NEG };

		Type type;
		std::string value;
	};

	std::string tenseTypeToString(Tense::Type  type);

	struct AfterObject
	{
		enum Type : bool { VERB, NOUN };

		const Type type;

		struct Verb
		{
			std::unique_ptr<ast::Verb> verb;
			std::unique_ptr<ast::Tense> tense;
			// period
		};
		struct Noun
		{
			std::unique_ptr<ast::Noun> noun;
			std::string destination;
			std::unique_ptr<ast::Verb> verb;
			std::unique_ptr<ast::Tense> tense;
			// period
		};

		union
		{
			Verb verb;
			Noun noun;
		};

		AfterObject(Verb&& verb) : type(Type::VERB), verb(std::move(verb)) {}
		AfterObject(Noun&& noun) : type(Type::NOUN), noun(std::move(noun)) {}

		~AfterObject()
		{
			switch (type)
			{
				case Type::VERB:
					verb.~Verb();
					return;
				case Type::NOUN:
					noun.~Noun();
					return;
			}
		}
	};

	struct AfterNoun
	{
		enum Type : short { BE, DESTINATION, OBJECT };

		const Type type;

		struct Be
		{
			std::unique_ptr<ast::Be> be;
			// period
		};
		struct Destination
		{
			std::string destination;
			std::unique_ptr<ast::Verb> verb;
			std::unique_ptr<ast::Tense> tense;
			// period
		};
		struct Object
		{
			std::string object;
			std::unique_ptr<AfterObject> afterObject;
		};

		union
		{
			Be be;
			Destination destination;
			Object object;
		};

		AfterNoun(Be&& be) : type(Type::BE), be(std::move(be)) {}
		AfterNoun(Destination&& destination) : type(Type::DESTINATION), destination(std::move(destination)) {}
		AfterNoun(Object&& object) : type(Type::OBJECT), object(std::move(object)) {}

		~AfterNoun()
		{
			switch (type)
			{
				case Type::BE:
					be.~Be();
					return;
				case Type::DESTINATION:
					destination.~Destination();
					return;
				case Type::OBJECT:
					object.~Object();
					return;
			}
		}
	};

	struct AfterSubject
	{
		enum Type : bool { VERB, NOUN };

		const Type type;

		struct Verb
		{
			std::unique_ptr<ast::Verb> verb;
			std::unique_ptr<ast::Tense> tense;
			// period
		};

		struct Noun
		{
			std::unique_ptr<ast::Noun> noun;
			std::unique_ptr<AfterNoun> afterNoun;
		};

		union
		{
			Verb verb;
			Noun noun;
		};

		AfterSubject(Verb&& verb) : type(Type::VERB), verb(std::move(verb)) {}
		AfterSubject(Noun&& noun) : type(Type::NOUN), noun(std::move(noun)) {}

		~AfterSubject()
		{
			switch (type)
			{
				case Type::VERB:
					verb.~Verb();
					return;
				case Type::NOUN:
					noun.~Noun();
					return;
			}
		}
	};

	struct S
	{
		bool hasConnector;
		std::string connector;

		std::unique_ptr<ast::Noun> noun;
		std::string subject;
		std::unique_ptr<AfterSubject> afterSubject;
	};

	struct Story
	{
		/// non-empty sequence of S
		std::deque<std::unique_ptr<S>> ss;

		Story(std::unique_ptr<S> s)
		{ ss.push_back(std::move(s)); }
	};

	/** Convert an AST node to json.
	 *
	 *  The schema is to use the field names
	 *  of the AST node fields as object keys in JSON.
	 *
	 *  Optionals, S::connector,
	 *  is ommitted if not present.
	 *
	 *  Sum types (tagged unions),
	 *  are encoded with a `"type"` JSON key
	 *  denoting the type prefixed by the struct and in pascal case.
	 *  So an AfterSubject node with type AfterSubject::Type::VERB
	 *  would have a string keyed by `"type"` with value `"AfterSubjectVerb"`.
	 *  The JSON object then would have the fields corresponding to that variant of node
	 *  unpacked into the JSON object.
	 *  Using the example above, AfterSubject::Verb::noun would be keyed by `"noun"`
	 *  and the value would be the JSON of the Noun node.
	 *
	 *  The tagged strings, Noun, Be, and Tense, are encoded mostly similarly like sum types
	 *  with them all having only JSON fields for `"type"` and `"value"`.
	 *  The names of the type field are not prefixed by the node name
	 *  So a Be node with type Be::Is would have the key `"type"`
	 *  have a string value of `"Is"`.
	 *
	 *  Verb is encoded by a simple one field object for its only field.
	 *
	 *  Use a std::ostringstream to obtain the JSON as a string
	 *  or std::cout to directly print the JSON to std out.
	 *  Any std::ostream can be used.
	 *
	 *  \param os output stream to output JSON to
	 */
	void toJson(std::ostream& os, const Story& story);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const S&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const AfterSubject& );

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const AfterNoun&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const AfterObject&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const Noun&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const Verb&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const Be&);

	/** Convert an AST node to json.
	 *  \see  toJson(std::ostream&, const Story&).
	 */
	void toJson(std::ostream&, const Tense&);
} }
