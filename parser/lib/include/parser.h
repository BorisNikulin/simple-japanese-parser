#pragma once

#include "token.h"
#include "token_stream.hpp"
#include "ast.h"

namespace sjp
{
	ast::Story parse(TokenStream<Token>& ts);

	namespace parser
	{
		ast::Story parseStory(TokenStream<Token>& ts);

		std::unique_ptr<ast::Noun> parseNoun(TokenStream<Token>& ts);
		std::unique_ptr<ast::Verb> parseVerb(TokenStream<Token>& ts);
		std::unique_ptr<ast::Be> parseBe(TokenStream<Token>& ts);
		std::unique_ptr<ast::Tense> parseTense(TokenStream<Token>& ts);
		std::unique_ptr<ast::AfterObject> parseAfterObject(TokenStream<Token>& ts);
		std::unique_ptr<ast::AfterNoun> parseAfterNoun(TokenStream<Token>& ts);
		std::unique_ptr<ast::AfterSubject> parseAfterSubject(TokenStream<Token>& ts);
		std::unique_ptr<ast::S> parseS(TokenStream<Token>& ts);
	}
}
