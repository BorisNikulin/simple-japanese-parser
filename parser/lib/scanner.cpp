#include "scanner.h"

#include "token.h"
#include "shape.hpp"

#include <algorithm>
#include <iterator>
#include <istream>
#include <string>
#include <unordered_map>
#include <utility>

namespace sjp
{
	std::pair<Token, std::string> scan(std::istream& in)
	{
		std::string word;
		in >> word; // get the next word
		return scan(word);
	}

	std::pair<Token, std::string> scan(const std::string& word)
	{
		Token token = scanner::tokenizeReservedWord(word); //return the token type for the word from reserved words

		if(token == Token::ERROR) // if the word wasn't a reserved word check if it is a period
		{
			token = scanner::tokenizePeriod(word); // get token type period
		}

		if(token == Token::ERROR) // if it wasn't a period check if it is WORD1 or WORD2
		{
			token = scanner::tokenizeWord(word); // get the other type
		}

		return std::make_pair(token, word); //return the word with its type
	}

	namespace scanner
	{

		Token tokenizePeriod(const std::string& word)
		{
			if (word == ".") // return period token type if the word is a period
			{
				return Token::PERIOD;
			}

			return Token::ERROR; // word isn't a period
		}

		Token tokenizeReservedWord(const std::string& word)
		{
			// list of all the reserved words
			const static std::unordered_map<std::string, const Token> reservedWords =
				{
					{"masu", Token::VERB},
					{"masen", Token::VERB_NEG},
					{"mashita", Token::VERB_PAST},
					{"masendeshita", Token::VERB_PAST_NEG},

					{"desu", Token::IS},
					{"deshita", Token::WAS},

					{"o", Token::OBJECT},
					{"wa", Token::SUBJECT},
					{"ni", Token::DESTINATION},

					{"watashi", Token::PRONOUN},
					{"anata", Token::PRONOUN},
					{"kanojo", Token::PRONOUN},
					{"sore", Token::PRONOUN},

					{"mata", Token::CONNECTOR},
					{"soshite", Token::CONNECTOR},
					{"shikashi", Token::CONNECTOR},
					{"dakara", Token::CONNECTOR},

					{"eofm", Token::EOFM}
				};

			auto it = reservedWords.find(word); // is word part of the reserved words

			if (it != reservedWords.end()) // the word is part of reserved words
			{
				return it->second; // return the type of the reserved word
			}
			else
			{
				return Token::ERROR; // not part of the reserved words return error
			}
		}

		Token tokenizeWord(const std::string& word)
		{
			enum State
			{
				q0 = 0, q1, qsa, qy, qc, qs, qt, q0q1, qyq0 // all of the possible states
			};

			// IIFE for complex static initialization
			static vector::ShapedArray<int, 9, 256> dfa = []()
				{
					vector::ShapedArray<int, 9, 256> dfa;

					std::fill(dfa.data().begin(), dfa.data().end(), -1); // initialize the dfa array with all invalid states

					// https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/nfta_to_dfa.md#dfa-transition-table
					// for all states that transition with a vowel check the transition
					for(int state : {q0, qsa, qy, qs, qt, q0q1, qyq0})
					{
						for(char c : "aeiouIE") // check if char c is in the vowels
						{
							dfa(state, c) = q0q1; // transition to the q0q1 state if char c was a vowel
						}
					}
					// for all states that transition with s to qs
					for(int state : {q0, q0q1, qyq0})
					{
						dfa(state, 's') = qs; // transition to qs
					}
					// for all states that transition with t to qt
					for(int state : {q0, q0q1, qyq0})
					{
						dfa(state, 't') = qt; // transition to qt
					}

					// for all states that transition with n to qy
					for(int state : {q0, qyq0})
					{
						dfa(state, 'n') = qy; // transition to qy
					}

					// check if q0q1 transitions with n to qyq0
					dfa(q0q1, 'n') = qyq0; // transition to qyq0

					// for q1 state check if it transitions with n to q0
					dfa(q1, 'n') = q0; // transition to q0

					// for all states that transition with c to qyq0
					for(int state : {q0, q0q1, qyq0})
					{
						dfa(state, 'c') = qyq0; // transition qyq0
					}


					// for all states that transition with y to qsa
					for(int state : {q0, qy, qt, q0q1, qyq0})
					{
						dfa(state, 'y') = qsa; // transition to qsa
					}

					// for all states that transition with h to qsa
					for(int state : {qc, qs})
					{
						dfa(state, 'h') = qsa; // transition to qsa
					}

					// for all states that transition with h to qy
					for(int state : {q0, q0q1, qyq0})
					{
						dfa(state, 'h') = qy; // transition to qy
					}

					// for all states that transition with chars dwzj to qy
					for(int state : {q0, q0q1, qyq0})
					{
						for(char c : "dwzj") // check if char c is in dwzj
						{
							dfa(state, c) = qy; // transition to qy
						}
					}

					// for all states that transition with chars bmkprg to qy
					for(int state : {q0, q0q1, qyq0})
					{
						for(char c : "bmkprg") // check if char is in bmkprg
						{
							dfa(state, c) = qy; // transition to qy
						}
					}

					// for all states that transition with s to qsa
					dfa(qt, 's') = qsa; // transition to qsa

					return dfa;
				}(); // multidementional table representing the dfa


			int state = q0; // start at initial state q0
			for(char c : word) // for each characters in the word
			{
				state = dfa(state, c); // get the current state
				if (state < 0) // check if state is in error state set earlier on
				{
					return Token::ERROR; // not a proper word
				}
			}

			auto finalStates = {q0, q0q1, qyq0}; // all possible final states

			// check if the state is one of the final states
			if(std::find(std::begin(finalStates), std::end(finalStates), state) != std::end(finalStates))
			{
				// check if the last character is IE
				if (std::string("IE").find(word.back()) != std::string::npos)
				{
					return Token::WORD2; // IE was found so it is WORD2
				}
				else
				{
					return Token::WORD1; // if it is not then its WORD1
				}
			}
			else
			{
				return Token::ERROR; // not WORD1 or WORD2 return an error
			}
		}
	}
}
