#include "parser.h"

#include "logging.h"
#include "token.h"
#include "token_stream.hpp"
#include "ast.h"

#include <iostream>
#include <memory>
#include <utility>
#include <set>

namespace sjp
{

	ast::Story parse(TokenStream<Token>& ts)
	{
		return parser::parseStory(ts);
	}

	namespace parser
	{
		// TODO: throws some error on trying to get a next/current token that doesnt exist
		// StreamExhausted error?
		// last token is fixed to be EOFM so stream exhastion would always haeppen
		// after a token mismatch unless we match EOFM and keep parsing
		// so this is probably unecessary for our parser funcs

		std::unique_ptr<ast::Noun> parseNoun(TokenStream<Token>& ts)
		{
			logTrace("Processing <noun>");

			switch (ts.nextToken({Token::WORD1, Token::PRONOUN})) // get next token or throw if not in list
			{
				case Token::WORD1:
					ts.matchToken(Token::WORD1);
					return std::unique_ptr<ast::Noun>(new ast::Noun{ast::Noun::Type::WORD1, ts.matchedString()});
				case Token::PRONOUN:
					ts.matchToken(Token::PRONOUN);
					return std::unique_ptr<ast::Noun>(new ast::Noun{ast::Noun::Type::PRONOUN, ts.matchedString()});
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
		}

		std::unique_ptr<ast::Verb> parseVerb(TokenStream<Token>& ts)
		{
			logTrace("Processing <verb>");

			ts.matchToken(Token::WORD2);
			return std::unique_ptr<ast::Verb>(new ast::Verb{ts.matchedString()});
		}

		std::unique_ptr<ast::Be> parseBe(TokenStream<Token>& ts)
		{
			logTrace("Processing <be>");

			switch (ts.nextToken({Token::IS, Token::WAS})) // get next token or throw if not in list
			{
				case Token::IS:
					ts.matchToken(Token::IS);
					return std::unique_ptr<ast::Be>(new ast::Be{ast::Be::Type::IS, ts.matchedString()});
				case Token::WAS:
					ts.matchToken(Token::WAS);
					return std::unique_ptr<ast::Be>(new ast::Be{ast::Be::Type::WAS, ts.matchedString()});
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
		}

		std::unique_ptr<ast::Tense> parseTense(TokenStream<Token>& ts)
		{
			logTrace("Processing <tense>");
			// get next token or throw if not in list
			switch (ts.nextToken({Token::VERB, Token::VERB_NEG, Token::VERB_PAST, Token::VERB_PAST_NEG}))
			{
				case Token::VERB:
					ts.matchToken(Token::VERB);
					return std::unique_ptr<ast::Tense>(new ast::Tense{ast::Tense::VERB, ts.matchedString()});
				case Token::VERB_NEG:
					ts.matchToken(Token::VERB_NEG);
					return std::unique_ptr<ast::Tense>(new ast::Tense{ast::Tense::VERB_NEG, ts.matchedString()});
				case Token::VERB_PAST:
					ts.matchToken(Token::VERB_PAST);
					return std::unique_ptr<ast::Tense>(new ast::Tense{ast::Tense::VERB_PAST, ts.matchedString()});
				case Token::VERB_PAST_NEG:
					ts.matchToken(Token::VERB_PAST_NEG);
					return std::unique_ptr<ast::Tense>(new ast::Tense{ast::Tense::VERB_PAST_NEG, ts.matchedString()});
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
		}

		std::unique_ptr<ast::AfterObject> parseAfterObject(TokenStream<Token>& ts)
		{
			logTrace("Processing <afterObject>");
			std::unique_ptr<ast::AfterObject> afterObject; // AfterObject struct

			// get next token or throw if not in list
			switch (ts.nextToken({Token::WORD2, Token::WORD1, Token::PRONOUN}))
			{
				case Token::WORD2:
				{
					ast::AfterObject::Verb afterObjectVerb;

					afterObjectVerb.verb = parseVerb(ts);
					afterObjectVerb.tense = parseTense(ts);

					ts.matchToken(Token::PERIOD);

					afterObject = std::unique_ptr<ast::AfterObject>(new ast::AfterObject(std::move(afterObjectVerb)));
					break;
				}
				case Token::WORD1:
				case Token::PRONOUN:
				{
					ast::AfterObject::Noun afterObjectNoun;

					afterObjectNoun.noun = parseNoun(ts);

					ts.matchToken(Token::DESTINATION);
					afterObjectNoun.destination = ts.matchedString();

					afterObjectNoun.verb = parseVerb(ts);
					afterObjectNoun.tense = parseTense(ts);

					ts.matchToken(Token::PERIOD);

					afterObject = std::unique_ptr<ast::AfterObject>(new ast::AfterObject(std::move(afterObjectNoun)));
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}

			return afterObject;
		}

		std::unique_ptr<ast::AfterNoun> parseAfterNoun(TokenStream<Token>& ts)
		{
			logTrace("Processing <afterNoun>");
			std::unique_ptr<ast::AfterNoun> afterNoun; // AfterNoun struct

			// get next token or throw if not in list
			switch (ts.nextToken({Token::IS, Token::WAS, Token::DESTINATION, Token::OBJECT}))
			{
				case Token::IS:
				case Token::WAS:
				{
					ast::AfterNoun::Be afterNounBe;

					afterNounBe.be = parseBe(ts);

					ts.matchToken(Token::PERIOD);

					afterNoun = std::unique_ptr<ast::AfterNoun>(new ast::AfterNoun(std::move(afterNounBe)));
					break;
				}
				case Token::DESTINATION:
				{
					ast::AfterNoun::Destination afterNounDestination;

					ts.matchToken(Token::DESTINATION);
					afterNounDestination.destination = ts.matchedString();

					afterNounDestination.verb = parseVerb(ts);
					afterNounDestination.tense = parseTense(ts);
					ts.matchToken(Token::PERIOD);

					afterNoun = std::unique_ptr<ast::AfterNoun>(new ast::AfterNoun(std::move(afterNounDestination)));
					break;
				}
				case Token::OBJECT:
				{
					ast::AfterNoun::Object afterNounObject;

					ts.matchToken(Token::OBJECT);
					afterNounObject.object = ts.matchedString();

					afterNounObject.afterObject = parseAfterObject(ts);
					afterNoun = std::unique_ptr<ast::AfterNoun>(new ast::AfterNoun(std::move(afterNounObject)));
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;

			}
			return afterNoun;
		}

		std::unique_ptr<ast::AfterSubject> parseAfterSubject(TokenStream<Token>& ts) {

			logTrace("Processing <afterSubject>");
			std::unique_ptr<ast::AfterSubject> afterSubject; // struct AfterSubject

			switch (ts.nextToken({Token::WORD2, Token::WORD1, Token::PRONOUN})) // get next token or throw if not in list
			{
				case Token::WORD2: // WORD2 is next token
				{
					ast::AfterSubject::Verb afterSubjectVerb;

					afterSubjectVerb.verb = parseVerb(ts); // store value of Verb or throw
					afterSubjectVerb.tense = parseTense(ts); // store value of Tense or throw
					ts.matchToken(Token::PERIOD); // Match period or throw

					// allows for returning multiple types
					afterSubject = std::unique_ptr<ast::AfterSubject>(new ast::AfterSubject(std::move(afterSubjectVerb)));
					break;
				}
				case Token::WORD1: // WORD1 Or PRONOUN are next tokens, which is a noun
				case Token::PRONOUN:
				{
					ast::AfterSubject::Noun afterSubjectNoun;
					afterSubjectNoun.noun = parseNoun(ts); // store noun or throw
					afterSubjectNoun.afterNoun = parseAfterNoun(ts); // store everything after parsing a noun or throw

					// allows for returning multiple types
					afterSubject = std::unique_ptr<ast::AfterSubject>(new ast::AfterSubject(std::move(afterSubjectNoun)));
					break;
				}
				default:
					// should not happen due to nextToken() throwing TokenUnexpected
					throw;
			}
			return afterSubject;
		}

		std::unique_ptr<ast::S> parseS( TokenStream<Token>& ts) {
			logTrace("Processing <s>");
			std::unique_ptr<ast::S> s(new ast::S{}); // struct s pointer
			// nextToken CONNECTOR is optional so it will not throw an error when not found
			if (ts.nextToken() == Token::CONNECTOR) // does CONNECTOR exist?
			{
				ts.matchToken(Token::CONNECTOR);
				s->hasConnector = true; // set hasConnector to true
				s->connector = ts.matchedString(); // get the actual connector and store it in s
			}
			else
			{
				s->hasConnector = false; // set hasConnector to false
			}

			//nextToken will throw an error if the token is not found
			//ts.nextToken({Token::WORD1, Token::PRONOUN});

			s->noun = parseNoun(ts); // extract the noun or throw
			ts.matchToken(Token::SUBJECT); // matches SUBJECT or throws
			s->subject = ts.matchedString(); // store matched value in s subject
			s->afterSubject = parseAfterSubject(ts); // get value of after subject or throw

			return s;
		}

		ast::Story parseStory( TokenStream<Token>& ts) {
			logTrace("Processing <story>");
			ast::Story story(parseS(ts)); //we push the results of parseS onto the deque that contains std::unique_ptr<S>

			while(ts.nextToken() != Token::EOFM) // check for EOFM token and continue if it isnt found
			{
				story.ss.push_back(parseS(ts)); // push the next parsed S onto the array
			}
			ts.matchToken(Token::EOFM); // Match EOFM token as it has to exist at the end

			return story;
		}
	}
}
