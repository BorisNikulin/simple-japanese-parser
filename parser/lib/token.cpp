#include "token.h"

#include <string>

namespace sjp {
	std::string tokenToString(const Token token)
	{
		static const std::string tokenStringTable[] =
			{
				"WORD1",
				"WORD2",
				"PERIOD",
				"VERB",
				"VERB_NEG",
				"VERB_PAST",
				"VERB_PAST_NEG",
				"IS",
				"WAS",
				"OBJECT",
				"SUBJECT",
				"DESTINATION",
				"PRONOUN",
				"CONNECTOR",
				"EOFM",
				"ERROR"
			};
		// returns the type of string
		return tokenStringTable[static_cast<int>(token)];
	}
}
