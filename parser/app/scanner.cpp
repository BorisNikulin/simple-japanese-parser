#include <scanner.h>
#include <token.h>

#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

namespace sjp {
	std::string usage(const char* progName)
	{
		std::string progNameStr = std::string(progName);
		return
			"Usage:\n"
			"\t" + progNameStr + " <input file>\n"
			"\t" + progNameStr + " - <words>\n"
			"\n"
			"Options:\n"
			"\t<input file>\t\tinput file path to scan or '-' to scan from stdin\n"
			"\t<words>     \t\twhitepace delimited string to scan the words of\n";
	}
}

int main(int argv, char* argc[])
{
	// take file name from args preferably
	std::ifstream inFile;
	std::istringstream inWords;
	std::istream* in = nullptr;

	if (argv == 2)
	{
		std::string arg(argc[1]);
		if (arg == "-h" || arg == "--help")
		{
			std::cout << sjp::usage(argc[0]);
			return 0;
		}
		else if (arg == "-")
		{
			in = &std::cin;
		}
		else
		{
			inFile.open(argc[1]);
			in = &inFile;
		}
	}
	else if (argv == 1)
	{
		std::string fileName;

		std::cout << "Enter the input file name: ";
		std::cin >> fileName;

		inFile.open(fileName);
		in = &inFile;
	}
	else if (argv == 3) // jank FFI for web server interop
	{
		// take input from 3rd argument and print just the token type
		std::string words(argc[2]);
		words += " eofm";
		inWords.str(words);
		in = &inWords;
	}
	else
	{
		std::cout << sjp::usage(argc[0]);
		return 0;
	}

	// run scanner func from library
	while (true)
	{
		auto tokenWordPair = sjp::scan(*in);

		if (tokenWordPair.first == sjp::Token::EOFM)
		{
			break;
		}

		if (argv != 3)
		{
			std::cout << "Type is: " << sjp::tokenToString(tokenWordPair.first) << "\n";
			std::cout << "Word is: " << tokenWordPair.second << "\n";
		}
		else
		{
			std::cout << sjp::tokenToString(tokenWordPair.first) << "\n";
		}
	}

	inFile.close();
	return 0;
}
