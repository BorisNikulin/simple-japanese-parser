#include <logging.h>
#include <token.h>
#include <scanner.h>
#include <parser.h>
#include <token_stream.hpp>
#include <ast.h>
#include <translator.h>

#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

namespace sjp {
	std::string usage(const char* progName)
	{
		std::string progNameStr = std::string(progName);
		return
			"Usage:\n"
			"\t" + progNameStr + " [<log>]\n"
			"\t" + progNameStr + " <log> <dictionary file> <input file>\n"
			"\t" + progNameStr + " <log> <dictionary file> - <words>\n"
			"\n"
			"Options:\n"
			"\t<log>            \t\tone of \"log\" or \"nolog\" to enable/disable logging, defaults to \"log\"\n"
			"\t<dictionary file>\t\tdictionary file to use for translation\n"
			"\t<input file>     \t\tinput file path to parse or '-' to parse from stdin\n"
			"\t<words>          \t\twhitepace delimited string to translate the words of\n";
	}
}

int main(int argc, char* argv[])
{
	sjp::logging::setupLoggingNoFilterClog();

	// take file name from args preferably
	std::ifstream dictionaryFile;
	std::ifstream inFile;
	std::istringstream inWords;
	std::istream* in = nullptr;

	if (argc == 4)
	{
		if (std::string("log") != argv[1])
		{
			sjp::logging::loggingConfig.filter = [](const sjp::Severity){ return false; };
		}

		dictionaryFile.open(argv[2]);

		if (std::string("-") == argv[3])
		{
			in = &std::cin;
		}
		else
		{
			inFile.open(argv[3]);
			in = &inFile;
		}
	}
	else if (argc == 1 || argc == 2)
	{
		if (argc == 2)
		{
			std::string arg(argv[1]);
			if (arg == "-h" || arg == "--help")
			{
				std::cout << sjp::usage(argv[0]);
				return 0;
			}
			else if (arg != "log")
			{
				sjp::logging::loggingConfig.filter = [](const sjp::Severity){ return false; };
			}
		}

		dictionaryFile.open("lexicon.txt");

		std::string fileName;

		std::cout << "Enter the input file name: ";
		std::cin >> fileName;

		inFile.open(fileName);
		in = &inFile;
	}
	else if (argc == 5) // jank FFI for web server interop
	{
		if (std::string("log") != argv[1])
		{
			sjp::logging::loggingConfig.filter = [](const sjp::Severity){ return false; };
		}

		dictionaryFile.open(argv[2]);

		std::string words(argv[4]);
		words += " eofm";
		inWords.str(words);
		in = &inWords;
	}
	else
	{
		std::cout << sjp::usage(argv[0]);
		return 0;
	}

	auto scanner = [](const std::string& word)
	{
		sjp::logTrace("Scanner called using word: " + word);
		return sjp::scan(word).first;
	};

	sjp::TokenStream<sjp::Token>
		ts(*in, *scanner, *sjp::tokenToString, sjp::Token::ERROR);

	const auto dictionary = sjp::parseDictionry(dictionaryFile);
	dictionaryFile.close();

	try
	{
		sjp::ast::Story story = sjp::parse(ts);
		sjp::translate(std::cout, dictionary, story);

	}
	catch (const sjp::TokenStreamError& e)
	{
		std::cout << e.what() << std::endl;
	}

	inFile.close();
	return 0;
}
