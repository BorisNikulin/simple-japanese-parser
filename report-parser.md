# G24 Parser Report


## Full Source

The full source is available at
[gitlab.com/simple-japanese-parser](https://gitlab.com/BorisNikulin/simple-japanese-parser).


## Web App

A web app version of the parser is available on
[heroku](https://simple-japanese-parser.herokuapp.com)
and utilizes `sjp-parser` to display a parsed syntax tree.


## Building

One can run the parser directly by
using the provided zip which is just the `parser` directory of the repo.

To initialize and build, run
```
unzip *.zip; cmake -Bbuild; cd build; make;
```

The build artifacts are in `build`
as `sjp-parser`.
To rebuild everything,
run `make` in the build directory.
`make help` will list some valid make targets.


## Documentation

Documentation is available in `docs/`
or online at the
[repository's pages](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp).

Documentation of the `sjp::parse` namespace that contains all parsing related code
is available
[here](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp/namespacesjp_1_1parser.html).


## Relevant Sources

The parser specific functions are in `lib/include/parser.h` and `lib/parser.cpp`.
The main is in `app/parse.cpp`.


## Program Status

The parser works as expected with no major bugs.
The output is slightly to the sample correct output
as the parser's output is modeled on the given specification
and then based on the sample correct output.

We added command line argument capability for convenience.
One can see the usage via `sjp-parse -h`.

If no arguments are provided,
the filename is asked for interactively and logging is enabled.

If exactly two arguments is provided,
the first argument determines whether logging is enabled.
the argument second arguments is used to determine the input stream.
The input stream is standard input if the argument is `-`
and the file with the given argument as file path if the argument is not `-`.

If there are three arguments,
the first arguments is used to enable or disable logging.
and the second argument is used as the text input source directly.
The output is the AST encoded to JSON outputted on stdout.
This usage is primarily for the web server.


## Contributions
* Boris
	* Parsing the bottom half of the grammar including:
		* parseAfterObject()
		* parseNoun()
		* parseVerb()
		* parseBe()
		* parseTense()
	* Top level documentation

* Maciek
	* Parsing the upper half of the grammar including:
		* parseStory()
		* parseS()
		* parseAfterSubject()
		* parseAfterNoun()
	* Comments
