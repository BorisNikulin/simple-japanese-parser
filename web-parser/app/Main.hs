{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Api

import Servant
import System.Environment (lookupEnv)
import Text.Read (readMaybe)
import Data.Maybe
import qualified Network.Wai.Handler.Warp as Warp

main :: IO ()
main = do
	port <- fmap (fromMaybe 8080 . (>>= readMaybe)) (lookupEnv "PORT")
	putStrLn $ "http://localhost:" ++ show port ++ "/"
	Warp.run port $ serve api server
