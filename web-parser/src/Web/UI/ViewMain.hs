{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveGeneric #-}

module Web.UI.ViewMain where

import Data.Ast
import Text.Scanner

import Data.List
import qualified Data.Foldable as F

import Data.Text (Text)
import qualified Data.Text as T
import Lucid
import Web.FormUrlEncoded
import GHC.Generics
import qualified Diagrams as D
import Diagrams.Backend.SVG
import qualified Graphics.Svg as Svg

defaultViewMain :: ViewMain
defaultViewMain = ViewMain (TextForm "soshite borisu wa toire ni ikI mashita .") Nothing

data ViewMain = ViewMain
	{ viewMainForm :: TextForm
	, viewMainResult :: Maybe ViewMainResult
	} deriving Show

data ViewMainResult = ViewMainResult
	{ viewMainResultTokenizedParagraph :: [[(Text, Token)]]
	, viewMainResultAst :: AstResult
	, viewMainResultTranslations :: [[Text]]
	} deriving Show

newtype TextForm = TextForm { text :: Text }
	deriving (Show, Generic)

instance FromForm TextForm

instance ToHtml TextForm where
	toHtml (TextForm formText) =  do
		form_ do
			with textarea_ [name_ "text", rows_ "20"] . toHtml $ formText
			with button_ [type_ "submit"] "Parse"
	toHtmlRaw = toHtml

instance ToHtml ViewMain where
	toHtml (ViewMain form result) = doctypehtml_ do
		head_ do
			meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1"]
			title_ "Simple Japanese Parser"
			link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/default.css"]
		body_ do
			with div_ [class_ "container"] do
				with div_ [class_ "content"] do
					h1_ "Parse Simple Japanese!"

					p_ do "The given input will be assigned to token types of"
						<> " words, reserved words, and a period."

					p_ "Words consist of the following regular expression:"

					p_ do "RE: (vowel | vowel n | consonant vowel | consonant vowel n |"
						 <> " consonant-pair vowel | consonant-pair vowel n)^+"

					p_ do "The difference between a WORD1 and a WORD2 is in the ending letter."
						<> " WORD2 words end in either an I or E while WORD1 type words do not."

					p_ "The rest of tokens correspond to reserved words or a single period."

					p_ do "Note: All input words must be whitespace delimited, even the periods."
						<> " Also, all input characters should be lowercase except for I and E."


					with (toHtml form) [method_ "post"]

					maybe
						mempty
						do \(ViewMainResult tokens astResult translations) -> do
							h2_ "Scanning"

							details_ do
								summary_ "Token Style Legend"
								ul_ . mconcat $ zipWith
									do \attributes tokenName -> with li_ attributes . toHtml $ T.pack tokenName
									do fmap tokenToClass [minBound..]
									do fmap show ([minBound..] :: [Token])

							with div_ [class_ "result"] . paragraphsWithMarkup $ (fmap . fmap . fmap) tokenToClass tokens

							h2_ "Parsing"

							case fromAstResult astResult of
								Right story ->
									toHtmlRaw
										. Svg.renderBS
										$ D.renderDia
											SVG
											do SVGOptions D.absolute Nothing "" [] True
											do storyDiagram story
								Left AstError -> p_ "Error in source" <> p_ "TODO: emit error in JSON and diplsay it here"

							h2_ "Translation"

							with ol_ [class_ "translations"]
								$ F.for_ translations \translationLines ->
									li_
										. with ul_ [class_ "translation"]
										. mconcat
										$ fmap (li_ . toHtml) translationLines

						result

				footer_ do
					with a_ [id_ "repo-link", href_ "https://gitlab.com/BorisNikulin/simple-japanese-parser"] do
						img_ [src_ "https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg"]
						"Repository"
	toHtmlRaw = toHtml

paragraphsWithMarkup :: Monad m => [[(Text, [Attribute])]] -> HtmlT m ()
paragraphsWithMarkup = foldMap (p_ . textWithMarkup)

-- | Wrap each word in a span with the given attributes or not if no attributes.
textWithMarkup :: Monad m => [(Text, [Attribute])] -> HtmlT m ()
textWithMarkup = mconcat . intersperse " " . fmap markupWithHtml where
	markupWithHtml (word, []) =  toHtml word
	markupWithHtml (word, attributes) =  with span_ attributes $ toHtml word

tokenToClass :: Token -> [Attribute]
tokenToClass token = (:[class_ "token "]) . class_ . T.toLower . T.pack $ show token
