{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}

module Web.Api
	( Api
	, api
	, server
	) where

import Text.Scanner
import Text.Parser
import Text.Translator
import Web.UI.ViewMain

import qualified Data.ByteString.Lazy.Char8 as LBS
import Control.Monad.IO.Class (liftIO)
import Servant
import Servant.HTML.Lucid

type Api
	= Get '[HTML] ViewMain
	:<|> ReqBody '[FormUrlEncoded] TextForm :> Post '[HTML] ViewMain
	:<|> "static" :> Raw

api :: Proxy Api
api = Proxy

server :: Server Api
server = formGet :<|>  formPost :<|> serveStatic

serveStatic :: Server Raw
serveStatic = serveDirectoryWebApp "static-files"

formGet :: Handler ViewMain
formGet = return defaultViewMain

formPost :: TextForm -> Handler ViewMain
formPost form = do
	tokenizedParagraph <- liftIO . scanner $ text form

	parseResult <- liftIO . parse $ text form
	astResult <- case parseResult of
		Left jsonError -> throwError err503 { errBody = "Invalid JSON returned from parser: " <> LBS.pack jsonError }
		Right astResult -> return astResult

	translation <- liftIO . translate $ text form

	return . ViewMain form . Just $ ViewMainResult tokenizedParagraph astResult translation
