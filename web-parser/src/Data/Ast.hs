{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}

module Data.Ast where

import Data.List.NonEmpty

import Data.Tree
import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.Aeson as A
import Data.Aeson ((.:), (.:?))
import Diagrams
import Diagrams.TwoD.Layout.Tree
import Diagrams.Backend.SVG
import Control.Lens hiding ((#))

type Connector = Text
type Subject = Text
type Destination = Text
type Object = Text
type Word2 = Text

data Noun = Word1 Text | Pronoun Text
	deriving Show

newtype Verb = Verb Word2
	deriving Show

data Be = Is Text | Was Text
	deriving Show

data Tense
	= VerbPresent Text
	| VerbNeg Text
	| VerbPast Text
	| VerbPastNeg Text
	deriving Show

data AfterObject
	= AfterObjectVerb Verb Tense
	| AfterObjectNoun Noun Destination Verb Tense
	deriving Show

data AfterNoun
	= AfterNounBe Be
	| AfterNounDestination Destination Verb Tense
	| AfterNounObject Object AfterObject
	deriving Show

data AfterSubject
	= AfterSubjectVerb Verb Tense
	| AfterSubjectNoun Noun AfterNoun
	deriving Show

data S = S (Maybe Connector) Noun Subject AfterSubject
	deriving Show

newtype Story = Story (NonEmpty S)
	deriving Show

newtype AstResult = AstResult { fromAstResult :: Either AstError Story }
	deriving Show

-- TODO: emit actual error from C++ and parse that JSON in
data AstError = AstError
	deriving Show

instance A.FromJSON Noun where
	parseJSON = A.withObject "Noun" \v -> do
		nounType <- v .: "type"
		nounValue <- v .: "value"
		case (nounType :: String) of
			"Word1" -> return $ Word1 nounValue
			"Pronoun" -> return $ Pronoun nounValue
			_ -> fail $ "invalid Noun type \"" ++ nounType ++"\""

instance A.FromJSON Verb where
	parseJSON = A.withObject "Verb" \v -> Verb <$> v.: "value"

instance A.FromJSON Be where
	parseJSON = A.withObject "Be" \v -> do
		beType <- v .: "type"
		beValue <- v .: "value"
		case (beType :: String) of
			"Is" -> return $ Is beValue
			"Was" -> return $ Was beValue
			_ -> fail $ "invalid Be type \"" ++ beType ++"\""

instance A.FromJSON Tense where
	parseJSON = A.withObject "Tense" \v -> do
		tenseType <- v .: "type"
		tenseValue <- v .: "value"
		case (tenseType :: String) of
			"Verb" -> return $ VerbPresent tenseValue
			"VerbNeg" -> return $ VerbNeg tenseValue
			"VerbPast" -> return $ VerbPast tenseValue
			"VerbPastNeg" -> return $ VerbPastNeg tenseValue
			_ -> fail $ "invalid Tense type \"" ++ tenseType ++"\""

instance A.FromJSON AfterObject where
	parseJSON = A.withObject "AfterObject" \v -> do
		afterObjectType <- v .: "type"
		case (afterObjectType :: String) of
			"AfterObjectVerb" ->
				AfterObjectVerb
					<$> v .: "verb"
					<*> v .: "tense"
			"AfterObjectNoun" ->
				AfterObjectNoun
					<$> v .: "noun"
					<*> v .: "destination"
					<*> v .: "verb"
					<*> v .: "tense"
			_ -> fail $ "invalid AfterObject type \"" ++ afterObjectType ++ "\""

instance A.FromJSON AfterNoun where
	parseJSON = A.withObject "AfterNoun" \v -> do
		afterNounType <- v .: "type"
		case (afterNounType :: String) of
			"AfterNounBe" ->
				AfterNounBe <$> v .: "be"
			"AfterNounDestination" ->
				AfterNounDestination
					<$> v .: "destination"
					<*> v .: "verb"
					<*> v .: "tense"
			"AfterNounObject" ->
				AfterNounObject
					<$> v .: "object"
					<*> v .: "afterObject"
			_ -> fail $ "invalid AfterNoun type \"" ++ afterNounType ++ "\""

instance A.FromJSON AfterSubject where
	parseJSON =  A.withObject "AfterSubject" \v -> do
		afterSubjectType <- v .: "type"
		case (afterSubjectType :: String) of
			"AfterSubjectVerb" ->
				AfterSubjectVerb
					<$> v .: "verb"
					<*> v .: "tense"
			"AfterSubjectNoun" ->
				AfterSubjectNoun
					<$> v .: "noun"
					<*> v .: "afterNoun"
			_ -> fail $ "invalid AfterSubject type \"" ++ afterSubjectType ++ "\""

instance A.FromJSON S where
	parseJSON = A.withObject "S" \v ->
		S
			<$> v .:? "connector"
			<*> v .: "noun"
			<*> v .: "subject"
			<*> v .: "afterSubject"

instance A.FromJSON Story where
	parseJSON = A.withObject "Story" \v ->
		Story <$> v .: "ss"

instance A.FromJSON AstResult where
	parseJSON = A.withObject "AstResult" \v -> do
		astResultType <- v .: "type"
		case (astResultType :: String) of
			"story" -> fmap (AstResult . Right) $ v .: "value"
			"error" -> return . AstResult $ Left AstError
			_ -> fail $ "invalid AstResult type \"" ++ astResultType ++ "\""

literalDiagram :: String -> Text -> Tree (Diagram SVG)
literalDiagram sType s =
	Node
		( do strutX 8 <> strutY 1.2 <> text sType
		  ===
		  do strutX 8 <> strutY 1.2 <> text (T.unpack s)
		) []

class ToDiagramTree a where
	toDiagramTree :: a -> Tree (Diagram SVG)

instance ToDiagramTree Story where
	toDiagramTree (Story ss) =
		Node
			do text "<story>" <> strutX 4 <> strutY 1.2
			do toDiagramTree <$> toList ss

instance ToDiagramTree S where
	toDiagramTree (S mayConnector noun subject afterSubject) =
		let
			connectorSubForest = case mayConnector of
				Just connector -> [literalDiagram "CONNECTOR" connector]
				Nothing -> []
		in
			Node
				do text "<s>" <> strutX 2 <> strutY 1.2
				( connectorSubForest
					++
					[ toDiagramTree noun
					, literalDiagram "SUBJECT" subject
					, toDiagramTree afterSubject
					]
				)

instance ToDiagramTree AfterSubject where
	toDiagramTree afterSubject =
		Node
			do text "<afterSubject>" <> strutX 8 <> strutY 12
			case afterSubject of
				AfterSubjectVerb verb tense ->
					[ toDiagramTree verb
					, toDiagramTree tense
					]
				AfterSubjectNoun noun afterNoun ->
					[ toDiagramTree noun
					, toDiagramTree afterNoun
					]

instance ToDiagramTree AfterNoun where
	toDiagramTree afterNoun =
		Node
			do text "<afterNoun>" <> strutX 7 <> strutY 1.2
			case afterNoun of
				AfterNounBe be ->
					[ toDiagramTree be
					]
				AfterNounDestination destination verb tense ->
					[ literalDiagram "DESTINATION" destination
					, toDiagramTree verb
					, toDiagramTree tense
					]
				AfterNounObject object afterObject ->
					[ literalDiagram "OBJECT" object
					, toDiagramTree afterObject
					]

instance ToDiagramTree AfterObject where
	toDiagramTree afterObject =
		Node
			do text "<afterObject>" <> strutX 7 <> strutY 1.2
			case afterObject of
				AfterObjectVerb verb tense ->
					[ toDiagramTree verb
					, toDiagramTree tense]
				AfterObjectNoun noun destination verb tense ->
					[ toDiagramTree noun
					, literalDiagram "DESTINATION" destination
					, toDiagramTree verb
					, toDiagramTree tense
					]

instance ToDiagramTree Noun where
	toDiagramTree noun =
		Node
			do text "<noun>" <> strutX 4.5 <> strutY 1.2
			case noun of
				Word1 word1 -> [literalDiagram "WORD1" word1]
				Pronoun pronoun -> [literalDiagram "PRONOUN" pronoun]

instance ToDiagramTree Verb where
	toDiagramTree (Verb word2) =
		Node
			do text "<verb>" <> strutX 4.5 <> strutY 1.2
			do [literalDiagram "WORD2" word2]

instance ToDiagramTree Be where
	toDiagramTree be =
		Node
			do text "<be>" <> strutX 3 <> strutY 1.2
			case be of
				Is is -> [literalDiagram "IS" is]
				Was was -> [literalDiagram "WAS" was]

instance ToDiagramTree Tense where
	toDiagramTree tense =
		Node
			do text "<tense>" <> strutX 4.5 <> strutY 1.2
			case tense of
				VerbPresent s -> [literalDiagram "VERB" s]
				VerbNeg s -> [literalDiagram "VERB_NEG" s]
				VerbPast s -> [literalDiagram "VERB_PAST" s]
				VerbPastNeg s -> [literalDiagram "VERB_PAST_NEG" s]

storyDiagram :: Story -> Diagram SVG
storyDiagram story =
	( renderTree id connectNodes
	. forceLayoutTree' (with & edgeLen .~ 6 & staticK .~ 2)
	. symmLayout' (with & slHSep .~ 4 & slVSep .~ 4)
	$ toDiagramTree story
	) # centerXY # pad 1.1
	where
		connectNodes :: P2 Double -> P2 Double -> Diagram SVG
		connectNodes pa pb = pa' ~~ pb' # lw 0.1 where
			pa' = let (x, y) = unp2 pa in p2 (x, y - 0.8)
			pb' = let (x, y) = unp2 pb in p2 (x, y + 0.5)

