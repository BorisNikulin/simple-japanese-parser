module Text.Parser where

import Data.Ast

import Data.Text (Text)
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as T
import System.Process
import Data.Aeson

parse :: Text -> IO (Either String AstResult)
parse source =
	let
		sjpParserConfig = proc "./sjp-parser"
			[ "nolog"
			, "-"
			, T.unpack source
			]
	in
		eitherDecodeStrict' . BS.pack <$> readCreateProcess sjpParserConfig ""
