{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}

module Text.Scanner where

import Data.Text (Text)
import qualified Data.Text as T
import System.Process

-- TyCon names are all caps to abuse derived instance of Read
-- for string output of Token in sjp-parser.
-- | Token type of a word.
-- Definitation lifted directly from libsjp parser.h in C++.
data Token
	= WORD1
	| WORD2

	| PERIOD

	| VERB
	| VERB_NEG
	| VERB_PAST
	| VERB_PAST_NEG

	| IS
	| WAS

	| OBJECT
	| SUBJECT
	| DESTINATION

	| PRONOUN
	| CONNECTOR

	| EOFM -- ^ can never be gotten from sjp-scanner as it stops the scanner

	| ERROR
	deriving (Show, Read, Enum, Bounded)

-- | Scans text an splits into paragraphs and words
-- and assigns each word a 'Token'.
scanner :: Text -> IO [[(Text, Token)]]
scanner =
	traverse
		do \paragraph -> zip paragraph <$> tokenizeWords paragraph
	. paragraphsAndWords

-- | Split a string by empty lines and then words.
--
-- Line endings are assumed to be DOS style.
paragraphsAndWords :: Text -> [[Text]]
paragraphsAndWords = fmap T.words . T.splitOn "\r\n\r\n" . T.strip

tokenizeWords :: [Text] -> IO [Token]
tokenizeWords wordsList =
	let
		sjpScannerConfig = proc "./sjp-scanner"
			[ "-"
			, T.unpack $ T.unwords wordsList
			]
	in
		fmap read . lines <$> readCreateProcess sjpScannerConfig ""
