{-# LANGUAGE OverloadedStrings #-}

module Text.Translator where

import Data.Text (Text)
import qualified Data.Text as T
import System.Process

translate :: Text -> IO [[Text]]
translate source =
	let
		sjpTranslatorConfig = proc "./sjp-translator-ast"
			[ "nolog"
			, "./lexicon.txt"
			, "-"
			, T.unpack source
			]
	in
		filter (/= []) . fmap T.lines . T.splitOn "\n\n" . T.pack <$> readCreateProcess sjpTranslatorConfig ""
