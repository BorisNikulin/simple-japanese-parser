PLANTUML ?= plantuml

PANDOC_FLAGS = -f markdown-implicit_figures

.PHONY: setup_web

setup_web:
	cmake ./parser -Bbuild
	cd build \
		&& make sjp-scanner \
		&& make sjp-parser \
		&& make sjp-translator-ast
	cp build/sjp-scanner ./web-parser
	cp build/sjp-parser ./web-parser
	cp build/sjp-translator-ast ./web-parser
	cp ./lexicon.txt ./web-parser

%.png: %.puml
	$(PLANTUML) -tpng $^

report-final.pdf: report-final.md dfa.png
	pandoc $(PANDOC_FLAGS) "$<" -o "$@"
