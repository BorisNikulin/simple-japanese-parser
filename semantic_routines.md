# Directions

Using semantic routines `#getEword#` and `#gen(TYPE)#`
generate IR from the simple Japanese parser.

`#getEword#` fetches the string associated with the last matched token,
attempts to translate the string using a provided dictionary,
and saves this resulting string.
If the dictionary cannot translate the string,
save the original string instead.

`#gen(TYPE)#` uses the last Eword and a given type of IR
to generate appropriate IR such as:

```
CONNECTOR:  However
ACTOR:      I/me
OBJECT:     okane
TO:         he/him
ACTION:     agE
TENSE:      VERBPASTNEG
```

## Semantic Routines Specification

```
For <s> Case 1:
<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)#
	<verb> #getEword# #gen(ACTION)#
	<tense> #gen(TENSE)#
	PERIOD

For <s> Case 2:
<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)#
	<noun> #getEword#
	<be> #gen(DESCRIPTION)# #gen(TENSE)#
	PERIOD

For <s> Case 3:
<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)#
	<noun> #getEword# DESTINATION #gen(TO)#
	<verb> #getEword# #gen(ACTION)#
	<tense> #gen(TENSE)# PERIOD

For <s> Case 4:
<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)#
	<noun> #getEword# OBJECT #gen(OBJECT)#
	<verb> #getEword# #gen(ACTION)#
	<tense> #gen(TENSE)#
	PERIOD

For <s> Case 5:
<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)#
	<noun> #getEword# OBJECT #gen(OBJECT)#
	<noun> #getEword# DESTINATION #gen(TO)#
	<verb> #getEword# #gen(ACTION)#
	<tense> #gen(TENSE)#
	PERIOD
```

After CONNECTOR is seen,
* #gen# generates the CONNECTOR: line

Right after SUBJECT is seen,
* #gen# generates the ACTOR:  line

Right after \<be\>,
* #gen# generates the DESCRIPTION: line
* #gen# generates the TENSE: line

Right after OBJECT is seen,
* #gen# generates the OBJECT: line

Right after DESTINATION is seen,
* #gen# generates the TO: line

After \<verb\> is seen,
* #gen# generates the ACTION: line

Right after \<tense\> is seen,
* #gen# generates the TENSE: line


# Left Factored Grammar

```
<story> ::= <s> { <s> }

<s> ::= [CONNECTOR] <noun> SUBJECT <afterSubject>

<afterSubject>
	::= <verb> <tense> PERIOD
	  | <noun> <afterNoun>

<afterNoun>
	::= <be> PERIOD
	  | DESTINATION <verb> <tense> PERIOD
	  | OBJECT <afterObject>

<afterObject>
	::= <verb> <tense> PERIOD
	  | <noun> DESTINATION <verb> <tense> PERIOD

<noun>  ::= WORD1 | PRONOUN
<verb>  ::= WORD2
<be>    ::= IS | WAS
<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
```


# Left Factored Grammar with Semantic Routines

```
<story> ::= <s> { <s> }

<s> ::=
	[CONNECTOR #getEword# #gen(CONNECTOR)#]
	<noun> #getEword# SUBJECT #gen(ACTOR)# <afterSubject>

<afterSubject>
	::= <verb> #getEword# #gen(ACTION)# <tense> #gen(TENSE)# PERIOD
	  | <noun> #getEword# <afterNoun>

<afterNoun>
	::= <be> #gen(DESCRIPTION)# #gen(TENSE)# PERIOD
	  | DESTINATION #gen(TO)# <verb> #getEword# #gen(ACTION)# <tense> #gen(TENSE)# PERIOD
	  | OBJECT #gen(OBJECT)# <afterObject>

<afterObject>
	::= <verb> #getEword# #gen(ACTION)# <tense> #gen(TENSE)# PERIOD
	  | <noun> #getEword# DESTINATION #gen(TO)# <verb> #getEword# #gen(ACTION)# <tense> #gen(TENSE)# PERIOD

<noun>  ::= WORD1 | PRONOUN
<verb>  ::= WORD2
<be>    ::= IS | WAS
<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
```
