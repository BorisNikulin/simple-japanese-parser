# Given Grammar

The given grammar rules in BNF form are

```
1 <story> ::= <s> { <s> }  // stay in the loop as long as a possible start 
                           // of <s> is the next_token  (note it can be CONNECTOR or WORD1 or PRONOUN)

2 <s> ::=  [CONNECTOR] <noun> SUBJECT  <verb> <tense> PERIOD 
3 <s> ::=  [CONNECTOR] <noun> SUBJECT  <noun> <be>    PERIOD 
4 <s> ::=  [CONNECTOR] <noun> SUBJECT  <noun> DESTINATION  <verb> <tense> PERIOD 
5 <s> ::=  [CONNECTOR] <noun> SUBJECT  <noun> OBJECT <verb> <tense> PERIOD 
6 <s> ::=  [CONNECTOR] <noun> SUBJECT  <noun> OBJECT <noun> DESTINATION <verb> <tense> PERIOD 
   // Refer to Left Factoring file to make these into
   // one rule until things start to differ.

7 <noun> ::= WORD1 | PRONOUN 
8 <verb> ::= WORD2

9 <be> ::=   IS | WAS

10 <tense> := VERBPAST  | VERBPASTNEG | VERB | VERBNEG
```

# Left Factoring

```
<s> ::= [CONNECTOR] <noun> SUBJECT <afterSubject>
<afterSubject>
    ::= <verb> <tense> PERIOD 
      | <noun> <be>    PERIOD 
      | <noun> DESTINATION  <verb> <tense> PERIOD 
      | <noun> OBJECT <verb> <tense> PERIOD 
      | <noun> OBJECT <noun> DESTINATION <verb> <tense> PERIOD 
```

```
<afterSubject>
    ::= <verb> <tense> PERIOD 
      | <noun> <afterNoun> 
<afterNoun>
    ::=  <be> PERIOD 
      | DESTINATION  <verb> <tense> PERIOD 
      | OBJECT <verb> <tense> PERIOD 
      | OBJECT <noun> DESTINATION <verb> <tense> PERIOD 
```

```
<afterNoun>
    ::= <be> PERIOD 
      | DESTINATION <verb> <tense> PERIOD 
      | OBJECT <afterObject>
<afterObject> 
    ::= <verb> <tense> PERIOD 
      | <noun> DESTINATION <verb> <tense> PERIOD
```


# Resulting Grammar

```
<story> ::= <s> { <s> }

<s> ::= [CONNECTOR] <noun> SUBJECT <afterSubject>

<afterSubject>
    ::= <verb> <tense> PERIOD 
      | <noun> <afterNoun> 

<afterNoun>
    ::= <be> PERIOD 
      | DESTINATION <verb> <tense> PERIOD 
      | OBJECT <afterObject>

<afterObject> 
    ::= <verb> <tense> PERIOD 
      | <noun> DESTINATION <verb> <tense> PERIOD

<noun>  ::= WORD1 | PRONOUN 
<verb>  ::= WORD2
<be>    ::= IS | WAS
<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
```
