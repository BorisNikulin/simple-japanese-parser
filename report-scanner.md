# G24 Scanner Report

## Full Source

The full source is available at
[gitlab.com/simple-japanese-parser](https://gitlab.com/BorisNikulin/simple-japanese-parser).

## Web App

A web app version of the scanner is available on
[heroku](https://simple-japanese-parser.herokuapp.com)
and utilizes the `sjp-scanner` for scanning needs.

## Building

One can run the scanner directly by
using the provided zip which is just the `parser` directory of the repo.

To initialize and build, run
```
unzip *.zip; cmake -Bbuild; cd build; make;
```

The build artifacts are in `build`
as `sjp-scanner`.
To rebuild everything,
run `make` in the build directory.
`make help` will list some valid make targets.


## Testing

You can run tests via ctest,
which is available on empress as `ctest3`,
without arguments to run all tests.


## Documentation

Documentation is available in `docs/`
or online at the
[repository's pages](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp).


## Relevant Sources

The scanner specific functions are in `lib/include/parser.h` and `lib/parser.cpp`.
The main is in `app/scanner.cpp`.


## Program Status

The scanner works as expected with no major bugs.
The output is different to the sample correct output
as the scanner's output is modeled on the provided main function.

We added command line argument capability for convenience.
If no arguments are provided,
the filename is asked for interactively.

If exactly one argument is provided,
the argument is used to determine the input stream.
The input stream is standard input if the argument is `-`
and the file with the given argument as file path if the argument is not `-`.

If there are two arguments,
the first arguments is ignored
and the second argument is used as the text input source directly.
This usage is primarily for the web server,
though, in hindsight, usage via stdin would have been sufficient.


## Contributions
* Boris

	Surrounding code around DFA including:
	* main in `scanner.cpp`
	* `ShapedArray` and related
	* `Token` enum
	* `tokenToString` with debugging help from Maciek
	* `tokenizePeriod()`
	* `tokenizeReservedWord()`
	* `scanner()`
	* top level doxygen documentation

* Maciek

	Worked on the main DFA including:
	* `tokenizeWord()`
	* detailed comments
