# Table of Contents
[[_TOC_]]


# Given NFA

The given NFA for tokenizing and parsing
simple Japanese roamji is

```plantuml
@startuml
hide empty description
title NFA

state "\n                      q0                      \n" as q0

q0 -up-> q0 : vowel
q0 --> q1 :vowel
q0 --> qsa : dwzyj
q0 --> qy : bmknhprg
q0 --> qt : t
q0 --> qs : s
q0 --> qc : c

q1 -> q0 : n

qsa --> q0 : vowel
qsa --> q1 : vowel

qy --> q0 : vowel
qy --> q1 : vowel
qy --> qsa : y

qt --> q0 : vowel
qt --> qsa : s
qt --> q1 : vowel

qs --> q0 : vowel
qs --> q1 : vowel
qs --> qsa : h

qc --> qsa : h

q0 -up-> [*]
[*] --> q0
@enduml
```


# NFA to DFA

## NFA Transitions

To convert an NFA to a DFA it helps to consider the transition functions.
We can order them by characters accepted.
This would mean Trs(state1, char) = state2 would be listed under char.

* vowel:
  * q0  -> {q0, q1}
  * q1  -> {}
  * qsa -> {q0, q1}
  * qy  -> {q0, q1}
  * qc  -> {}
  * qs  -> {q0, q1}
  * qt  -> {q0, q1}
* s:
  * q0  -> {qs}
  * q1  -> {}
  * qsa -> {}
  * qy  -> {}
  * qc  -> {}
  * qs  -> {}
  * qt  -> {qsa}
* t:
  * q0 -> {qt}
  * the rest go to {}
* y:
  * q0 -> {qsa}
  * qy -> {qsa}
  * the rest go to {}
* n:
  * q0 -> {qy}
  * q1 -> {q0}
  * the rest go to {}
* c:
  * q0 -> {qc}
  * the rest go to {}
* h:
  * q0 -> {qy}
  * qc -> {qsa}
  * qs -> {qsa}
  * the rest go to {}
* dwzj:
  * q0 -> {qsa}
  * the rest go to {}
* bmkprg:
  * q0 -> {qy}
  * the rest go to {}

So the transition for q0 on a t would be {qt}
or $`\Delta(q0,t) = \{qt\}`$.


## DFA Transitions

For each NFA state we turn the resulting set
into a DFA state by treating all the states together as a new state
or ignoring the transition if the set is empty.

For example,

```math
\Delta(q0,\text{vowel}) = \{q0, q1\}
```

would result in the DFA transition

```math
\delta(q0,\text{vowel}) = q0q1
```

where q0q1 is now a new state all on it's own.

* vowel:
  * q0  -> q0q1
  * qsa -> q0q1
  * qy  -> q0q1
  * qs  -> q0q1
  * qt  -> q0q1
* s:
  * q0 -> qs
  * qt -> qsa
* t:
  * q0 -> qt
* y:
  * q0 -> qsa
  * qy -> qsa
* n:
  * q0 -> qy
  * q1 -> q0
* c: 
  * q0 -> qc
* h:
  * q0 -> qy
  * qc -> qsa
  * qs -> qsa
* dwzj:
  * q0 -> qsa
* bmkprg:
  * q0 -> qy

The process resulted in a new state q0q1.
We must now list all transitions for this new state.
One can find such a transition by taking each NFA transition making up the new DFA state
and unioning all resulting NFA states and making a combined DFA state.
In other words

```math
\delta(q0q1,\text{vowel})
  = \text{stateFrom}(\Delta(q0,\text{vowel}) \cup \Delta(q1,\text{vowel})) \\
= \text{stateFrom}(\{q0, q1\} \cup \varnothing) \\
= \text{stateFrom}(\{q0, q1\}) \\
= q0q1
```

* vowel:
    * q0q1 -> q0q1
* s:  
    * q0q1 -> qs
* t:
    * q0q1 -> qt
* y:
    * q0q1 -> qsa
* n:
    * q0q1 -> qyq0
* c:
    * q0q1 -> qc
* h:
    * q0q1 -> qy
* dwzj:
    * q0q1 -> qsa
* bmkprg:
    * q0q1 -> qy

Another new state qyq0 was created.

* vowel:
    * qyq0 -> q0q1
* s:
    * qyq0 -> qs
* t:
    * qyq0 -> qt
* y:
    * qyq0 -> qsa
* n:
    * qyq0 -> qy
* c:
    * qyq0 -> qc
* h:
    * qyq0 -> qy
* dwzj:
    * qyq0 -> qsa
* bmkprg:
    * qyq0 -> qy


### DFA Transition Table

The DFA transitions as a table are

state | vowel | s  | t    | y    | n    | c    | h    | dwzj | bmkprg 
-----|------|------|------|------|------|------|------|------|--------
q0   | q0q1 | qs   | qt   | qsa  | qy   | qc   | qy   | qsa  | qy
q1   |      |      |      |      | q0   |      |      |      |
qsa  | q0q1 |      |      |      |      |      |      |      |
qy   | q0q1 |      |      | qsa  |      |      |      |      |
qc   |      |      |      |      |      |      | qsa  |      |
qs   | q0q1 |      |      |      |      |      | qsa  |      |
qt   | q0q1 | qsa  |      | qsa  |      |      |      |      |
q0q1 | q0q1 | qs   | qt   | qsa  | qyq0 | qc   | qy   | qsa  | qy
qyq0 | q0q1 | qs   | qt   | qsa  | qy   | qc   | qy   | qsa  | qy


## DFA Diagram

The DFA for tokenizing and parsing
simple Japanese romaji is

```plantuml
@startuml
hide empty description
title DFA

state "\n             q0             \n" as q0
state "\n            q0q1            \n" as q0q1
state "\n             qsa             \n" as qsa


[*] --> q0
q0 --> [*]
q0q1 -up-> [*]
qyq0 -up-> [*]

q0 -> q0q1 : vowel
q0 --> qs : s
q0 --> qt : t
q0 --> qc : c
q0 --> qsa : dwzyj
q0 --> qy : bmkprg,nh

qsa --> q0q1 : vowel

qy --> q0q1 : vowel
qy --> qsa : y

qs --> q0q1 : vowel
qs --> qsa : h

qt --> q0q1 : vowel
qt --> qsa : s

q1 --> q0 : n

qc --> qsa : h

q0q1 --> q0q1 : vowel
q0q1 --> qs : s
q0q1 --> qt : t
q0q1 --> qyq0 : n
q0q1 --> qc : c
q0q1 --> qsa : dwzyj
q0q1 --> qy : bmkprg,h

qyq0 --> q0q1 : vowel
qyq0 --> qs : s
qyq0 --> qt : t

qyq0 --> qc : c

qyq0 --> qsa : dwzyj
qyq0 --> qy : bmkprg,nh

@enduml
```

The q1 state can never be reached and can be removed.

```plantuml
@startuml
hide empty description
title DFA

state "\n             q0             \n" as q0
state "\n            q0q1            \n" as q0q1
state "\n             qsa             \n" as qsa

[*] --> q0
q0 --> [*]
q0q1 -up-> [*]
qyq0 -up-> [*]

q0 -> q0q1 : vowel
q0 --> qs : s
q0 --> qt : t
q0 --> qc : c
q0 --> qsa : dwzyj
q0 --> qy : bmkprg,nh

qsa --> q0q1 : vowel

qy --> q0q1 : vowel
qy --> qsa : y

qs --> q0q1 : vowel
qs --> qsa : h

qt --> q0q1 : vowel
qt --> qsa : s

qc --> qsa : h

q0q1 --> q0q1 : vowel
q0q1 --> qs : s
q0q1 --> qt : t
q0q1 --> qyq0 : n
q0q1 --> qc : c
q0q1 --> qsa : dwzyj
q0q1 --> qy : bmkprg,h

qyq0 --> q0q1 : vowel
qyq0 --> qs : s
qyq0 --> qt : t

qyq0 --> qc : c

qyq0 --> qsa : dwzyj
qyq0 --> qy : bmkprg,nh

@enduml
```
