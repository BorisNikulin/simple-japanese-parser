# G24 Translator Report


## Full Source

The full source is available at
[gitlab.com/simple-japanese-parser](https://gitlab.com/BorisNikulin/simple-japanese-parser).


## Web App

A web app version of the parser is available on
[heroku](https://simple-japanese-parser.herokuapp.com)
and utilizes `sjp-translator-ast` to display a translation


## Building

One can run the parser directly by
using the provided zip which is just the `parser` directory of the repo.

To initialize and build, run
```
unzip *.zip; cmake -Bbuild; cd build; make;
```

The build artifacts are in `build`
as `sjp-translator` and `sjp-translator-ast.
To rebuild everything,
run `make` in the build directory.
`make help` will list some valid make targets.


## Documentation

Documentation is available in `docs/`
or online at the
[repository's pages](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp).

Documentation of the `sjp::translator` namespace that contains all translation related code
is available
[here](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp/namespacesjp_1_1translator.html).


## Relevant Sources

The translation specific functions are in `lib/include/translator.h` and `lib/translator.cpp`.
The main is in `app/translator.cpp` and `app/translator_ast.cpp`.


## Program Status

The translator works as expected with no major bugs.
The output is very similar to the sample correct output
with some whitespace and logging differences.

We added command line argument capability for convenience.
One can see the usage via `sjp-translate -h`.

If no arguments are provided,
the filename is asked for interactively,
the dictionary file is assumed to be `./lexicon,txt",
and logging is enabled.

If exactly three arguments is provided,
the first argument determines whether logging is enabled.
The second argument determines the location of the dictionary to use for translation
and is a white space separated file of a pair of Romaji Japanese word and it's English translation.
The third argument is used to determine the input stream.
The input stream is standard input if the argument is `-`
and the file with the given argument as file path if the argument is not `-`.

If there are four arguments,
the first argument is used to enable or disable logging.
The second argument is used for the dictionary file as described above.
The fourth argument is used as the text input source directly
with an implicit "eofm" word added.


## Contributions

* Boris
	* getEword()
	* gen()
	* Translation including:
		* translateAfterSubject()
		* translateAfterNoun()
		* AST translation functions
	* Top level documentation

* Maciek
	* Translation including:
		* translateStory()
		* translateS()
		* translateAfterObject()
	* Comments
