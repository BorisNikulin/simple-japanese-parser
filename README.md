# Simple Japanese Parser (Theory of Computing)


## Index

* [Web Site](https://simple-japanese-parser.herokuapp.com)
* [Project Page](https://borisnikulin.gitlab.io/simple-japanese-parser)
* [Parser Docs](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/sjp)
* [Server Docs](https://borisnikulin.gitlab.io/simple-japanese-parser/docs/web)
* [Repo](https://gitlab.com/BorisNikulin/simple-japanese-parser)


## Documents
* [NFA to DFA](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/nfa_to_dfa.md)
* [Left Factoring of Grammar](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/left_factor_grammar.md)
* [Grammar with Semantic Routines](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/left_factor_grammar.md)


## Reports
* [Scanner Report](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/report-scanner.md)
* [Parser Report](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/report-parser.md)
* [Translator Report](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/report-translator.md)


## Description

The projects uses elements of computing theory
such as deterministic finite automatons (DFA), recursive descent parser, and semantic routines
to translate a restricted set of Japanese written in Romaji or ascii.

Elements of the parser are all written in C++
and achieve scanning, parsing, and code generation.

The web portion of presenting the parser is written in Haskell.


## Scanning

Scanning was achieve by taking a provided specification with its non-deterministic finite automaton (NFA)
and convert the NFA to a DFA by hand.
The process was written down
[here](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/left_factor_grammar.md).

Then using the DFA,
we implemented the DFA using a table driven approach.


## Parsing

Parsing started as a basic grammar
which we took and left factored to allow recursive descent parsing.
The process was written down
[here](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/nfa_to_dfa.md).

Once the input was scanned into tokens,
we then implemented recursive descent functions for each non-terminal
and parsed that into an AST representation of the grammar.


## Semantic Routines and Code/Intermediate Representation (IR) Generation

Lastly, we we're given a simple Japanese to English dictionary
along with a specification of what to generate and when.
The conversion of the semantic routines to left factored semantic routines was written down
[here](https://gitlab.com/BorisNikulin/simple-japanese-parser/-/blob/master/semantic_routines.md)

Once we knew what routines to run and when,
we altered parsing to also generate the correct IR.
Alternatively, since we also had an AST result of the grammar,
we also did IR generation via inspecting the AST
which allowed separation of parsing and generation.


## Web

The web server then takes each of these programs and runs them on input
to generate the HTML served to the clients.
The parser can emit JSON to aid reconstructing the tree structure.
